from . base import *

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2', # slouzi pro komunikaci Django - postgreSQL
#         'NAME': 'explorea', # takto nazvana databaze musi byt jiz vytvorena v postgreSQL a jeji owner musi byt user o radek nize
#         'USER': 'majkl', # tento user se musi shodovat se jmenem usera, pod kterym bezi operacni system na mem PC, zaroven musi byt stejny user jiz vytvoreny v postgreSQL
#         'PASSWORD': 'supersecretpassword', # toto heslo musi byt jiz nastavene pro usera o radek vyse v postgreSQL
#         'HOST': '',
#         'PORT': '',
#     }
# }

# SECRET_KEY = get_env_variable('SECRET_KEY')

ALLOWED_HOSTS = ["*"]
