import re
from django.core.exceptions import ValidationError


def validate_human_name(value):

    regex = r'^[A-Za-z\s\-]+$'
    if not re.match(regex, value):
        raise ValidationError(
            'Names can contain only alpha characters',
            code='invalid')
"""
def validate_date(value): #regex musi byt datetime.date

    regex = r'^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$'# 00-31.00-12.1900-2099
    if not re.match(regex, value):
        raise ValidationError(
            'Do not enter nonsense',
            code='invalid')
"""

"""
def validate_time(value):

    pass
"""
