"""eventos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from explorea.events import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', views.index, name='index'), # path vraci objekt URLPattern <URLPattern "..nejaky regularni vyraz.." [name=".."]>
    path('', views.IndexView.as_view(), name='index'),
    path('events/', include('explorea.events.urls', namespace="events")), #nyni muzeme v templatech referencovat url jako {% url "namespace:name" "var"%}, takze ruzne aplikace mohou mit stejne name (myapp/urls.py path(...,name=...))
    path('accounts/', include('explorea.accounts.urls', namespace="accounts")),
    path('cart/', include('explorea.cart.urls', namespace='cart')),
    path('posts/', include('explorea.posts.urls', namespace='posts')),
]


if settings.DEBUG: # pokud je pouzit testovaci Django server - pri ostrem provozu se pouziva k obsluhovani images separatni nezavisly server
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) # static funkce spoji obe casti cesty v jeden celek (vraci list objektu URLPattern <URLPattern "..nejaky regularni vyraz.."> )
