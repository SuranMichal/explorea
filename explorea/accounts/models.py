from django.db import models
from django.conf import settings

from django.urls import reverse

def image_dir(instance, filename):
    return 'user_{}/profile_{}'.format(instance.user.id, filename) # vraci dynamicky generovanou cast url (kazdy user bude mit vlastni slozku s uploadovanymi obrazky)
                                                                    # - prvni argument je id usera, druhy je jmeno jim uploadovaneho profiloveho obrazku
class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, # vytvori jednoznacnou referenci na User model (primary_key=True znamena, ze v User modelu zase pribude field profile s jednoznacnou referenci na model Profile)
        on_delete=models.CASCADE, primary_key=True)
    is_host = models.BooleanField(default=False)
    about = models.TextField(max_length=500)
    avatar = models.ImageField('Upload your photo', upload_to=image_dir, null=True) # upload_to - cesta k obrazku (str), null=True - neni povinne vlozit obrazek

    def get_absolute_url(self):
            return reverse('accounts:host_profile', args=[self.user.username]) # self - instance Profile (ten, na ktery user klikne), vrati napriklad http://127.0.0.1:8000/accounts/host/profile/ab/

# cela cesta k obrazku bude napr. explorea/media/user_1/profile_photo.jpg ... explorea/media/... tato cast definovana v settings.py, po uploadu na server: http://127.0.0.1:8000//media/user_1/profile_photo.jpg
# Django spoleha na nainstalovany modul Pillow (ve virtualnim prostredi) pri praci s obrazky

# # kod do shell - pro vsechny jiz existujici user objekty: overi, zda maji atribut profile, pokud ne (Exception), tak vytvori objekt profile (a do user atributu/fieldu ulozi user objekt)
# for user in User.objects.all():
#     try:
#         getattr(user, 'profile')
#     except Exception:
#         Profile.objects.create(user=user)


# # kod do shell pro overeni automatickeho vytvoreni profilu userovi
# from django.contrib.auth.models import User
# u = User.objects.create(username="Franz", email="franz@example.com", password='password123')
# u.profile   -->     <Profile: Profile object (9)>
#
# from explorea.accounts.models import Profile
# p = Profile.objects.get(pk=9)
# p.user      -->      <User: Franz>
# u.delete()   -->        (2, {'admin.LogEntry': 0, 'auth.User_groups': 0, 'auth.User_user_permissions': 0, 'accounts.Profile': 1, 'auth.User': 1})
# # protoze mame u attr user definovano : on_delete=models.CASCADE --> vymaze se timto nejen user, ale i jeho profile
