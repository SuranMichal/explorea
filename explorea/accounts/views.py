from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, update_session_auth_hash, get_user_model
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse

# from django.contrib.auth.mixins import LoginRequiredMixin # LoginRequiredMixin byl z dedicnosti odstranen po zavedeni custom middleware
from django.views.generic import TemplateView, DetailView, ListView, View
from django.views.generic.base import TemplateResponseMixin, ContextMixin
from django.views.generic.edit import CreateView, UpdateView, FormMixin, ProcessFormView
from django.urls import reverse_lazy

from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes

from .forms import RegisterForm, EditUserForm, EditProfileForm
from .models import Profile

UserModel = get_user_model()
token_generator = PasswordResetTokenGenerator()


# @login_required
#
# def profile(request):
#
#     return render(request, 'accounts/profile.html')


class ProfileView(TemplateView):

    template_name = 'accounts/profile.html'


# def register(request):
#
#     if request.method == 'POST':
#
#         # create the form and populate it with data from the POST request
#         form = RegisterForm(request.POST)
#
#         # validate the data in the form
#         if form.is_valid():#vytvoren atribut form.cleaned_data (type=dict) a validovana data do nej ulozena
#
#
#             # get the validated data from the form
#             username = form.cleaned_data.get('username')# nazvy poli formulare jsou ve slovniku jako "key" (type=str)
#             email = form.cleaned_data.get('email')
#             first_name = form.cleaned_data.get('first_name')
#             last_name = form.cleaned_data.get('last_name')
#             password = form.cleaned_data.get('password1')
#
#             # create and save the user
#             User = get_user_model() # ziska referenci k nami zvolenemu user modelu (settings.AUTH_USER_MODEL, nachazi se v general_settings.py a Djanguv defaultni je auth.User)
#             u = User(username=username,
#                      email=email,
#                      first_name=first_name,
#                      last_name=last_name)
#             u.set_password(password)#pouze tato metoda ulozi password hash
#             u.save()
#
#             # get the validated data from the form, create and save the user
#             user=form.save()# protoze implementujeme RegisterForm jako ModelForm--> lze pouzit save --> vrati user object jako radek, ktery zapise do databaze
#             raw_password = form.cleaned_data.get('password1')
#
#             #user = authenticate(username=username, password=password) #vyhleda username v databazi, zahashuje argument password a porovna s odpovidajicim v databazi, vrati objekt user
#             user = authenticate(username=user.username, password=raw_password)
#             login(request, user) #vytvori user session
#
#             # redirect to another page
#             return redirect('accounts:profile')#parametrem je view, ktery chceme spustit, Django default - settings.LOGIN_REDIRECT_URL (="/accounts/profile")
#
#         else: # form.is_valid() vrati False, viz def change_password
#             return render(request, 'accounts/register.html', {'form': form} )
#
#     # if the request is of type GET
#     form = RegisterForm()
#     return render(request, 'accounts/register.html', {'form': form} )


class RegisterView (CreateView):
    model = UserModel
    form_class = RegisterForm
    template_name = 'accounts/register.html'

    #success_message = 'The user %(username)s has been registered successfully' # property pouzivane MessageActionMixin metodami
    #error_message = 'The user %(username)s can not be registered'

    def form_valid(self, form):
        user = form.save()
        raw_password = form.cleaned_data.get('password1')
        user = authenticate(username=user.username, password=raw_password)
        login(self.request, user)

        #super().form_valid(form)

        return redirect('accounts:profile')


# @login_required
# def edit_profile(request):
#
#     user_form = EditUserForm(request.POST or None, instance=request.user) #  pokud request.method == 'GET': request.POST je <QueryDict {}>, pokud request.method == 'POST': request.POST je <QueryDict {"csrfmiddlewaretoken": ["..."], "username":["Michal"], ..., "about":["neco"]}>
#     profile_form = EditProfileForm(request.POST or None, request.FILES or None, instance=request.user.profile)#instance urcuje jaka data maji byt ve formulari predvyplnena # musi byt dodrzene poradi requestu text, files
#                                                                                                             #  pokud request.method == 'POST', request.POST nyni obsahuje field: "avatar":["nazev_obrazku.png"]
#     if request.method == 'POST':
#
#         if user_form.is_valid() and profile_form.is_valid():
#             user = user_form.save()
#             profile = profile_form.save()
#
#             request.session['profile_changes'] = request.session.setdefault('profile_changes', 0) + 1#do request.session objektu (lze s nim pracovat jako se slovnikem) se ulozi pod klicem profile_changes defautni hodnota 0,
#                                                                                    #ktera se pri kazdem ulozeni formulare do databaze ve views.edit_profile zvysi o 1
#             return redirect('accounts:profile')
#
#     return render(request, 'accounts/edit_profile.html',
#         {'user_form': user_form, 'profile_form': profile_form})


class MultipleFormsMixin (ContextMixin):
    forms_classes = {}
    initial = {}
    success_url = "" # atributy budou overridovany view, ktere z mixinu dedi

    def get_forms_classes (self):

        return self.forms_classes.copy() # copy() - puvodni dict forms_classes zustane nezmenen v pripade, ze se zmeni novy dict (ktery vraci get_forms_classes()), jinak by se pripadne zmeny dotkly obou
                                        # vezme to, co je definovano v EditProfileView.forms_classes a pokracuje v MFM.get_forms(): forms = self.get_forms_classes()
    def get_context_data(self, **kwargs):

        if 'forms' not in kwargs:
            forms = self.get_forms() # GET: forms = {'user_form': <EditUserForm bound=False, valid=Unknown, fields=(username;email;first_name;last_name;password)>, 'profile_form': <EditProfileForm bound=False, valid=Unknown, fields=(about;avatar)>}

            for name, form in forms.items():
                kwargs[name] = form # ulozi napr. kwargs["user_form"]= <EditUserForm bound=False, valid=Unknown, fields=(username;email;first_name;last_name;password)>
                                    # ve vysledku kwargs==forms

            return super().get_context_data(**kwargs) # ContextMixin.get_context_data()


#        else:
#            keys = ("user_form", "profile_form")
#            for index,value in enumerate(kwargs["forms"]):
#                kwargs[keys[index]]=value
#            kwargs.pop ("forms")


    def get_forms_kwargs(self):
        kwargs = {}
        for name, form in self.forms_classes.items():
            kwargs[name] = {"initial": self.get_initial(),}
            if self.request.method in ('POST', 'PUT'):
                kwargs[name].update({'data': self.request.POST, 'files': self.request.FILES,})

        return kwargs

    def get_forms(self, forms_classes = None):
        if forms_classes is None:
            forms = self.get_forms_classes() # GET, POST: forms = {'user_form': <class 'explorea.accounts.forms.EditUserForm'>, 'profile_form': <class 'explorea.accounts.forms.EditProfileForm'>}

        kwargs = self.get_forms_kwargs() # GET: kwargs = {'user_form': {'initial': {}, 'instance': <SimpleLazyObject: <User: ab>>}, 'profile_form': {'initial': {}, 'instance': <Profile: Profile object (1)>}}
                                        # POST: kwargs = {'user_form': {'initial': {}, 'data': <QueryDict: {'csrfmiddlewaretoken': ['..'], 'username': ['ab'], 'email': ['a@b.cz'], 'first_name': ['Acko'], 'last_name': ['Becko'], 'about': ['dwdwd']}>, 'files': <MultiValueDict: {'avatar': [<InMemoryUploadedFile: ..>]}>, 'instance': <SimpleLazyObject: <User: ab>>},
                                        #              'profile_form': {'initial': {}, 'data': <QueryDict: {'csrfmiddlewaretoken': ['..'], 'username': ['ab'], 'email': ['a@b.cz'], 'first_name': ['Acko'], 'last_name': ['Becko'], 'about': ['dwdwd']}>, 'files': <MultiValueDict: {'avatar': [<InMemoryUploadedFile: ..>]}>, 'instance': <Profile: Profile object (1)>}}
                                        # odkazuje na EditProfileView.get_forms_kwargs()

        return {name:form(**kwargs[name]) for name, form in forms.items()} # dict.items() vraci [("key", value), ("key", value), ... ], **dict - rozbali pary key:value jako kwargs (var=value) pri volani funkce
                                                                        # napr. pro 'user_form' vraci:
                                                                        #    GET: forms['user_form'](**kwargs['user_form']) --> <EditUserForm bound=False, valid=Unknown, fields=(username;email;first_name;last_name;password)>
                                                                        #   POST:                                           --> <EditUserForm bound=True, valid=Unknown, fields=(username;email;first_name;last_name;password)>
                                                                        # GET: forms['user_form'](**kwargs['user_form']) odpovida: forms['user_form'](initial=None, instance= self.request.user)
    def forms_valid (self, forms):
        return HttpResponseRedirect(self.get_success_url()) # presmeruje na profil usera

    def forms_invalid (self, forms):

        return self.render_to_response(self.get_context_data(forms=forms)) # odkazuje na MFM.get_context_data(), ale "forms" je v kwargs, proto jde do else (muj vytvor) a dale do ContextMixin.get_context_data()
                                                                            # puvodne (bez else v MFM.get_context_data()): self.get_context_data(forms=forms) --> {'forms': dict_values([<EditUserForm bound=True, valid=False, fields=(..)>, <EditProfileForm bound=True, valid=Unknown, fields=(..)>]), 'view': <explorea.accounts.views.EditProfileView object at 0x00000000040C2D68>}
                                                                            # nyni                                                                            --> {'user_form': <EditUserForm bound=True, valid=False, fields=(..)>, 'profile_form': <EditProfileForm bound=True, valid=True, fields=(..)>, 'view': <explorea.accounts.views.EditProfileView object at 0x00000000040E4C50>}
                                                                            # odkazuje na TemplateResponseMixin.render_to_response(), ktera vraci stejnou stranku s vyplnenym formem a errory
    def get_initial (self):
        return self.initial.copy()

    def get_success_url(self):
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)

class MultipleModelFormsMixin (MultipleFormsMixin):

    instances = {}

    def dispatch(self, request, *args, **kwargs):
        self.instances = self.get_instances() # vrati self.instances = {'user_form': None, 'profile_form': None}
        return super().dispatch(request, *args, **kwargs) # spusti LoginRequiredMixin.dispatch() (verifikace, zda je user authenticated) - ten obsahuje taky super().dispatch() (odkazuje na View.as_view() - self.dispatch), ktery spusti zakladni View.dispatch(), ktery spusti ProcessMultipleFormsView.get() nebo ProcessMultipleFormsView.post()
    def get_instances (self):
        instances = {}
        for name in self.forms_classes:
            instances[name] = None
        return instances

    def forms_valid(self, forms):

        for form in forms:
            form.save()
        return super().forms_valid(forms) # odkazuje na MultipleFormsMixin.forms_valid()

    def get_forms_kwargs (self):
        kwargs = super().get_forms_kwargs() # odkazuje na MultipleFormsMixin.get_forms_kwargs()
        for name, form in self.forms_classes.items():
            kwargs[name].update ({"instance": self.instances[name]})
        return kwargs

class ProcessMultipleFormsView(MultipleFormsMixin, TemplateResponseMixin, ProcessFormView):
    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data()) # odpovida ProcessFormView.get()
                                                                # odkazuje na MultipleFormsMixin.get_context_data()
                                                                # odkazuje na TemplateResponseMixin.render_to_response()
    def post(self, request, *args, **kwargs):

        self.forms = self.get_forms() # odkazuje na MultipleFormsMixin.get_forms()
                                     # self.forms =  {'user_form': <EditUserForm bound=True, valid=Unknown, fields=(username;email;first_name;last_name;password)>, 'profile_form': <EditProfileForm bound=True, valid=Unknown, fields=(about;avatar)>}

        if all ([form.is_valid() for form in self.forms.values()]):

            return self.forms_valid (self.forms.values()) # odkazuje na MultipleModelFormsMixin.forms_valid()
        else:

            return self.forms_invalid (self.forms.values()) # odkazuje na MultipleFormsMixin.forms_invalid()

class ProcessMultipleModelFormsView (MultipleModelFormsMixin, ProcessMultipleFormsView):
    pass

class EditProfileView (ProcessMultipleModelFormsView):

    forms_classes = {"user_form": EditUserForm, "profile_form": EditProfileForm} # sem si sahne MFM.get_forms_classes()
    template_name = 'accounts/edit_profile.html' # sem si sahne TemplateResponseMixin.render_to_response()
    success_url = reverse_lazy ('accounts:profile') # sem si sahne MFM.get_success_url()

    def get_forms_kwargs (self): # GET: tuto metodu spousti MFM.get_forms(): kwargs = self.get_forms_kwargs()
        kwargs = super().get_forms_kwargs()  # odkazuje na MultipleModelFormsMixin.get_forms_kwargs(); GET: kwargs = {'user_form': {'initial': {}, 'instance': None}, 'profile_form': {'initial': {}, 'instance': None}}
                                             # GET: metoda MFM vytvori kwargs slovnik a klic initial (value None), metoda MMFM prida instance (value None), teprve metoda EPV naplni kwargs klic instance objekty User a Profile
        kwargs ["user_form"].update({"instance":self.request.user})
        kwargs ["profile_form"].update({"instance":self.request.user.profile})
        return kwargs


# @login_required
# def change_password(request):
#
#     if request.method == 'POST':
#         form = PasswordChangeForm(data=request.POST, user=request.user)
#
#         if form.is_valid():
#             user = form.save()
#             update_session_auth_hash(request, user)
#             return redirect('accounts:profile')
#         else: # nyni mame pristup k self.errors, abychom je mohli dat do template, predtim se ztratily, protoze nasledovalo nacteni prazdneho formulare
#             return render(request, 'accounts/change_password.html', {'form': form})
#
#     form = PasswordChangeForm(user=request.user)#user - required keyword argument
#     return render(request, 'accounts/change_password.html', {'form': form})


class PasswordChangeView(UpdateView):
    model = UserModel
    form_class = PasswordChangeForm
    template_name = 'accounts/change_password.html'
    success_url = reverse_lazy ('accounts:profile')

    def form_valid(self,form):
        self.object = form.save()
        update_session_auth_hash(self.request, self.object)
        return HttpResponseRedirect(self.get_success_url())
        # return redirect ('accounts:profile') funguje taky, nahrazuje success_url a HttpResponseRedirect

    def get_form_kwargs (self):
        kwargs = super().get_form_kwargs()
        kwargs.pop('instance') # SetPasswordForm (parent PasswordChangeFormu) pri inicializaci chce kwargs self, user, instance nezna ??
        kwargs.update({'user': self.object })
        return kwargs

    def get_object(self):
        return self.request.user


# @login_required
# def host_list(request):
#     profiles = Profile.objects.filter(is_host=True)
#     if request.user.profile.is_host: # pokud je requestujici user hostitelem, nebude zobrazen na host_list.html
#         profiles = profiles.exclude(user__pk=request.user.id)
#
#     return render(request, 'accounts/host_list.html', {'profiles':profiles})


class HostListView (ListView):
    model = Profile
    template_name = 'accounts/host_list.html'
    context_object_name = 'profiles'

    def get_queryset(self):
        qs = self.model._default_manager.filter(is_host=True)
        return qs.exclude(user__pk=self.request.user.id) # pokud je requestujici user hostitelem, nebude zobrazen na host_list.html


# @login_required
# def host_profile(request, username):
#     host = UserModel.objects.get(username=username)
#     return render(request, 'accounts/host_profile.html', {'host': host})


class HostProfileView (DetailView):
    model = UserModel
    template_name = 'accounts/host_profile.html'
    context_object_name = 'host'
    #slug_url_kwarg =  'username' # tento a nasledujici radek - reseni engeto; .. nazev parametru z URLconf, ktery obsahuje slug
    #slug_field =  'username' .. nazev fieldu na modelu, ktery obsahuje slug

    def get_object(self):
        host = UserModel.objects.get(username=self.kwargs["username"])
        return host


# @login_required
# def become_host(request):
#     verification_email_template = 'accounts/host_verification_email.html'
#     email_context = {
#         'user': request.user,
#         'domain': get_current_site(request).domain, # get_current_site() vraci RequestSite objekt, ktery definuje attr domain (vrati napr. "127.0.0.1:8000")
#         'uidb64': urlsafe_base64_encode(force_bytes(request.user.pk)).decode(), # 'uidb64' - user.id zakodovane do base64, urlsafe_base64_encode() pozaduje bytes objekt --> force bytes() zaridi konverzi
#         'token': token_generator.make_token(request.user) # class PasswordResetTokenGenerator definuje make_token()
#     }
#
#     html_body = render_to_string(verification_email_template, email_context) # input jsou 2 argumenty: 1) nazev/cesta k template, 2) kontext - promenne, ktere se do template vlozi; vraci string
#     subject = 'Explorea Host Verification'
#     from_email = 'admin@explorea.com'
#     to_email = request.user.email
#
#     email = EmailMessage(subject, html_body, from_email, [to_email]) # EmailMessage - kontejner pro emaily; ma v __init__() po rade: subject="", body="", from_email=None, to=None, aj.; definuje tez metodu send()
#     email.send()
#
#     return render(request, 'accounts/verification_sent.html')


class BecomeHostView (TemplateView):
    template_name = 'accounts/verification_sent.html'

    def get(self, request, *args, **kwargs):
        verification_email_template = 'accounts/host_verification_email.html'
        email_context = {
            'user': request.user,
            'domain': get_current_site(request).domain, # get_current_site() vraci RequestSite objekt, ktery definuje attr domain (vrati napr. "127.0.0.1:8000")
            'uidb64': urlsafe_base64_encode(force_bytes(request.user.pk)).decode(), # 'uidb64' - user.id zakodovane do base64, urlsafe_base64_encode() pozaduje bytes objekt --> force bytes() zaridi konverzi
            'token': token_generator.make_token(request.user) # class PasswordResetTokenGenerator definuje make_token()
                        }

        html_body = render_to_string(verification_email_template, email_context) # input jsou 2 argumenty: 1) nazev/cesta k template, 2) kontext - promenne, ktere se do template vlozi; vraci string
        subject = 'Explorea Host Verification'
        from_email = 'admin@explorea.com'
        to_email = request.user.email

        email = EmailMessage(subject, html_body, from_email, [to_email]) # EmailMessage - kontejner pro emaily; ma v __init__() po rade: subject="", body="", from_email=None, to=None, aj.; definuje tez metodu send()
        email.send()

        return super().get(self, request, *args, **kwargs)

def activate_host(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode() # dekoduje uidb64 na uid=user.id
        user = UserModel._default_manager.get(pk=uid) # vybere toho usera z db, jehoz pk odpovida uid
    except (TypeError, ValueError, OverflowError, User.DoesNotExist): # pokud ho nenajde ...
        user = None

    if user is not None and token_generator.check_token(user, token): # pokud user existuje a jeho token je validni (token byl vytvoren z user atributu (last login, password), proto k overeni je treba user objekt)
        user.profile.is_host = True # user se stava hostitelem
        user.profile.save()
        return render(request, 'accounts/verification_complete.html')

    else:
        return render(request, 'accounts/invalid_link.html') # zmena doby platnosti tokenu dajngo/conf/global_settings.py - PASSWORD_RESET_TIMEOUT_DAYS
                                                             # spravny nazev by mohl byt v tomto pripade VERIFICATION_TOKEN_TIMEOUT_DAYS, protoze se obecne tyka tokenu, ktery overuje uzivatele, at uz je ucelem reset hesla, nebo neco jineho

class ActivateHostView (TemplateView):
    template_name = ('accounts/invalid_link.html', 'accounts/verification_complete.html')

    def get(self, request, *args, **kwargs):
        try:
            uidb64 = self.kwargs["uidb64"]
            token = self.kwargs["token"]
            uid = urlsafe_base64_decode(uidb64).decode() # dekoduje uidb64 na uid=user.id
            user = UserModel._default_manager.get(pk=uid) # vybere toho usera z db, jehoz pk odpovida uid
        except (TypeError, ValueError, OverflowError, User.DoesNotExist): # pokud ho nenajde ...
            user = None

        if user is not None and token_generator.check_token(user, token):
            user.profile.is_host = True
            user.profile.save()
            self.kwargs.update({'success': "True" }) # reseni engeto - vse je dale stejne jako u activate_host(), jen je pod get (..)
        else:
            self.kwargs.update({'success': "False" })

        return super().get(self, request, *args, **kwargs)

    def get_template_names(self):

        if self.kwargs["success"] == "True":
            return [self.template_name[True]]
        else:
            return [self.template_name[False]]

# class ValidateUsernameView(View):
#
#     def post(self, request, *args, **kwargs):
#         username = self.request.POST['username']
#         data = {
#             'exists': UserModel.objects.filter(username=username).exists()
#         }
#
#         if data['exists']: # pokud data['exists'] == True, tj. pokud existuje user v nasi DB, ktery je shodny s userem v request.POST, do slovniku data se pod klic error message ulozi dana zprava
#             data['error_message'] = 'The username already exists'
#
#         return JsonResponse(data)   # nevraci nic na server (neni render()/template_name), ale primo v klientovi/browseru
#                                     # data musi byt dict (pokud safe=True coz je default)
#                                     # prevadi dict na JSON (napr. pokud response = JsonResponse(data) --> vysledek je k dispozici v response.content)

class ValidateFieldExistValueView(View):

    def post(self, request, *args, **kwargs):

        name = self.request.POST['field_name'] # JSON bude mit nyni strukturu napr. {'field_name': "email", "field_value" : "jean@example.com", "csrfmiddlewaretoken" : "..."}
        value = self.request.POST['field_value'] # vraci napr. "Michal"

        # kdyz zadam value do pole username, prekliknu a zadam value do pole email, prekliknu:
            # poslou se dva ruzne request.POST
            # prvni request.POST -> napr. <QueryDict: {'field_name': ['username'], 'field_value': ['Michal'], 'csrfmiddlewaretoken': ['KbtG6r..']}>,
            # druhy request.POST -> napr. <QueryDict: {'field_name': ['email'], 'field_value': ['a@b.cz'], 'csrfmiddlewaretoken': ['KbtG6r..']}>
            # to, jak vypada request.POST, definuje $.ajax(), atribut data, v js/main.js

        data = {
            'exists': self.model.objects.filter(**{name: value}).exists() # self.model.objects.filter(**{name: value}) -> napr. self.model.objects.filter('username'="ab"}) -> <QuerySet [<User: ab>]>, nebo <QuerySet []>
        }

        if data['exists']:
            data['error_message'] = 'The {} {} already exists'.format(*(name, value)) # *(name, value) -> name, value

        return JsonResponse(data)

class ValidateRegisterFormView(ValidateFieldExistValueView): # tato class, ktera dedi od predchozi genericke, ma za ukol definovat ta pole, ktera budou validovana
    validated_fields = ['username', 'email']
    model = UserModel
