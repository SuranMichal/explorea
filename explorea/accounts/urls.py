from django.urls import path
from django.conf.urls import url

from django.contrib.auth import views as auth_views

from . import views

app_name = 'accounts' #to se propoji s config.urls kde je definovan namespace=...

urlpatterns = [
    #path('profile/', views.profile, name='profile'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    #path('login/', auth_views.login, {'template_name': 'accounts/login.html'}, name='login'),
    path('login/', auth_views.LoginView.as_view(template_name="accounts/login.html"), name='login'),
    #path('logout/', auth_views.LogoutView.as_view(template_name="accounts/logged_out.html"), name='logout')
    path('logout/', auth_views.LogoutView.as_view(next_page="/events/all/"), name='logout'),
    #path('register/', views.register, name='register'),
    path('register/', views.RegisterView.as_view(), name='register'),
    #path('profile/edit/', views.edit_profile, name='edit_profile'),
    path('profile/edit/', views.EditProfileView.as_view(), name='edit_profile'),

    #path('hosts/', views.host_list, name='host_list'),
    path('hosts/', views.HostListView.as_view(), name='host_list'),
    #path('become-host/', views.become_host, name='become_host'),
    path('become-host/', views.BecomeHostView.as_view(), name='become_host'),
    #path('host/profile/<username>/', views.host_profile, name='host_profile'),
    path('host/profile/<username>/', views.HostProfileView.as_view(), name='host_profile'),
    #url(r'^become-host/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.activate_host, name='activate_host'),
    url(r'^become-host/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.ActivateHostView.as_view(), name='activate_host'),

    # <uidb64> a <token> se zparsuji do views.activate_host jako kwargs;
    # ?P<uidb64> je Python named capture group (<uidb64> je refernece na [0-9A-Za-z]+, muze byt backreferencovana jako (?P=uidb64)), . = libovolny znak

    #path('change-password/', views.change_password, name='change_password'),
    path('change-password/', views.PasswordChangeView.as_view(), name='change_password'),
    path('reset-password/', auth_views.PasswordResetView.as_view(template_name = 'accounts/reset_password.html', email_template_name='accounts/reset_password_email.html'),
        name='reset_password'),
    path('reset-password/done/', auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done'),
    url(r'^reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),
    path('reset-password/complete/', auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'),

    # path('ajax/validate_username/', views.ValidateUsernameView.as_view(), name='validate_username'),
    path('ajax/validate_exists/', views.ValidateRegisterFormView.as_view(), name='validate_register_form'), # nahrazuje predchozi path

    ]
