from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


UserModel = get_user_model()

class EmailBackend(ModelBackend): # overridujeme metodu authenticate() ModelBackendu, protoze chceme usera autentizovat pomoci pole email, ne username

    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD) # v django.contrib.auth.models.BaseUser je defaultne USERNAME_FIELD = 'username'
                                                            # kdybychom vytvareli custom model User, stacilo by prepsat USERNAME_FIELD = 'email'
        try:
            user = UserModel._default_manager.get(email=username) # zde je jedina odlisnost vuci ModelBackend: user = UserModel._default_manager.get_by_natural_key(username)
                                                                # get_by_natural_key() je v django.contrib.auth.base_user.BaseUserManager (obecny model a jeho USERNAME_FIELD)
                                                                # na UserModel je field email, kteremu posleme value username, coz je jeden ze vstupu do authenticate()
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            UserModel().set_password(password) # redundatni kod - snazi se vyrovnat rozdil v dobe reakce DB mezi existujicim a neexistujicim userem, aby hacker nepoznal podle kratsi odezvy, ze se trefil
        else:
            if user.check_password(password) and self.user_can_authenticate(user): # user_can_authenticate() overi, ze user existuje a je aktivni - konkretne zamitne usera, ktery ma is_active = False
                return user
