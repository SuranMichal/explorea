import re

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, UserModel
from django.core.exceptions import ValidationError

from config.core.validators import validate_human_name #, validate_date

from .models import Profile

class NameField(forms.CharField):

    def to_python (self, value):
        name = super().to_python(value)
        return name

    def validate (self, value):
        regex = r'^[A-Za-z\s\-]+$'
        if not re.match(regex, value):
            raise ValidationError(
                'Names can contain only alpha characters',
                code='invalid')

class RegisterForm(UserCreationForm):#(forms.ModelForm)...generate forms from models#(forms.Form)..Form dedi od BaseForm

    #username = forms.CharField(max_length=30, help_text='How will we call you?')# <input type="text"
    #email = forms.EmailField()# <input type="email".../implementace v django.forms.fields
    #first_name = forms.CharField(max_length=100)#first_name = forms.CharField(max_length=100, widget=forms.Textarea)..pokud bychom chtěli <textarea ... instead of <input type='text'
    #last_name = forms.CharField(max_length=100)

    #django nezareaguje v pripade ze password1 nesouhlasi s password2 --> to za nas resi preddefinovany UserCreationForm (jedna se tez o ModelForm)-->
    # password1 = forms.CharField( #fields, ktere nema model User musime nadefinovat, ostatni (vyse) ma
    #     label="Password",
    #     strip=False,
    #     widget=forms.PasswordInput, #located at: django.forms.templates.django.forms.widgets, pristupne pres forms.DanyWidget
    # )
    # password2 = forms.CharField(
    #     label="Password confirmation",
    #     widget=forms.PasswordInput,
    #     strip=False,
    #     help_text="Enter the same password as before, for verification.",
    # )

    first_name = forms.CharField(max_length=100, validators=[validate_human_name,])
    last_name = forms.CharField(max_length=100, validators=[validate_human_name,])

    class Meta:#pokud dedime z forms.ModelForm, django bude hledat v class RegisterForm class Meta
        model = get_user_model() #z jakeho modelu se vygeneruji form fields
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
        ]#optional, jake fields chceme aby mel form # exclude=[]..vynechani fields

class EditUserForm(UserChangeForm): #UserChangeForm dedi od ModelForm, ktery je definovan v django.forms.models, labely pro jednotliva fields jsou definovane

    first_name = NameField(max_length=100)
    last_name = NameField(max_length=100)

    class Meta:
        model = UserModel #v django.contrib.auth.forms je: UserModel = get_user_model()
        fields = [
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
        ]

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['about', "avatar"]


#     def clean (self):
#         cleaned_data = super().clean()
#         first_name= cleaned_data.get ("first_name")
#         last_name= cleaned_data.get ("last_name")
#         regex = r'^[A-Za-z\s\-]+$'
#         msg = 'Names can contain only alpha characters'
#
#         for key, name in [("first_name", first_name),("last_name", last_name)]:
#             if not re.match (regex, name):
#                 raise ValidationError(
#                     'Names can contain only alpha characters',
#                     code='invalid') # ukladaji se do form.non_field_errors !!
#
#     def clean (self):
#         cleaned_data = super().clean()
#         first_name= cleaned_data.get ("first_name")
#         last_name= cleaned_data.get ("last_name")
#         regex = r'^[A-Za-z\s\-]+$'
#         msg = 'Names can contain only alpha characters'
#
#         for key, name in [("first_name", first_name),("last_name", last_name)]:
#             if not re.match (regex, name):
#                 self.add_error(key, msg) # ukladaji se do field.errors !!
#
#     def clean_first_name(self):
#         name=self.cleaned_data("first_name")
#         regex = r'^[A-Za-z\s\-]+$'
#
#         if not re.match (regex, name):
#             raise ValidationError(
#                 'Names can contain only alpha characters',
#                 code='invalid')
#         return name
#
#     def clean_last_name(self):
#         name=self.cleaned_data("last_name")
#         regex = r'^[A-Za-z\s\-]+$'
#
#         if not re.match (regex, name):
#             raise ValidationError(
#                 'Names can contain only alpha characters',
#                 code='invalid')
#         return name
