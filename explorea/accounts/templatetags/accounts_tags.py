from django import template

register = template.Library()

@register.filter
def has(obj, attr_name): # obj je <ImageFieldFile>, tzn File objekt, attr_name je "url" , coz je atribut class FieldFile, resp. Storage # parent classes lze zjistit: type(obj).__bases__ popr. type(obj).__bases__[0].__bases__

    try:
        return bool(getattr(obj, attr_name))
    except ValueError:
        return False
