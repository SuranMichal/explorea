from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_save

# kod v tomto file se spousti vzdy pri spusteni serveru/python shell (??)
# aby se trida AccountsConfig loadovala, je potreba ji zaregistrovat v __init__.py jako: default_app_config = 'explorea.accounts.apps.AccountsConfig'

class AccountsConfig(AppConfig):
    name = 'explorea.accounts'

    def ready(self): # spusti se, kdyz spustime server (python manage.py runserver)
        import explorea.accounts.signals as signals

        post_save.connect(signals.create_profile, sender=settings.AUTH_USER_MODEL) # registruje nami definovanou funkci create_profile jako signal typu post_save
                                                                                    # tzn. pokazde kdy spusteno user.save() --> spusti se create_profile (protoze sender odkazuje na User model)
