from django.conf import settings

from unittest.mock import Mock

from django.test import TestCase, RequestFactory
from django.urls import reverse

from .. middleware import LoginMiddleware, TestMiddleware
from explorea.events.views import MyEventsView, EventListView

def get_request(url, is_authenticated):
    request_factory = RequestFactory()
    request = request_factory.get(url) # RequestFactory.get() vytvori fake GET request; do get() vstupuje URL testovane stranky
    request.user = Mock() # vytvori fake request.user
    request.user.is_authenticated = is_authenticated  # vytvori fake instanci AnonymousUser # takto muzeme u Mock objektu definovat cokoliv

    return request

class LoginMiddlewareTest(TestCase):

    def setUp(self):
        self.middleware = LoginMiddleware(None) # custom middleware
        self.non_login_view = EventListView.as_view() # View testovane stranky
        self.login_required_view = MyEventsView.as_view()
        self.login_required_url = reverse('events:my_events_listing')
        self.non_login_url = reverse('events:events_listing', args=['all'])

    def test_non_login_required_view_not_intercepted_for_anonymous_user(self): # user prihlasen: NE, stranka vyzaduje login: NE

        request = get_request(self.non_login_url, False)

        response = self.middleware.process_view(request,
                            self.non_login_view, (), {})

        self.assertIsNone(response)

    def test_login_required_view_returns_login_redirect_for_anonymous_user(self): # user prihlasen: NE, stranka vyzaduje login: ANO

        request = get_request(self.login_required_url, False)

        response = self.middleware.process_view(request,
                       self.login_required_view, (), {})

        self.assertEquals(response.status_code, 302) # test, zda response je typu HttpResponseRedirect (HttpResponseRedirect.status_code == 302)
        self.assertEquals(response.url, settings.LOGIN_URL) # response.url odkazuje na HttpResponseRedirect.url, coz je URL stranky, na kterou se redirectuje # settings.LOGIN_URL je '/accounts/login/'

    def test_non_login_required_view_not_intercepted_for_loggedin_user(self): # user prihlasen: ANO, stranka vyzaduje login: NE

        request = get_request(self.non_login_url, True)

        response = self.middleware.process_view(request, self.non_login_view, (), {})

        self.assertIsNone(response)


    def test_login_required_view_not_intercepted_for_loggedin_user(self): # user prihlasen: ANO, stranka vyzaduje login: ANO

        request = get_request(self.login_required_url, True)

        result = self.middleware.process_view(request, self.login_required_view, (), {})

        self.assertIsNone(result)
