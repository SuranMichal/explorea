from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse

# from ..views import ValidateUsernameView
from ..views import ValidateRegisterFormView

UserModel = get_user_model()

class UserLoginMixin: # tento mixin vytvori usera pro testcase a prihlasi ho na zacatku kazdeho testu

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username,
                                                 password=cls.password)

    def setUp(self):
        super().setUp()
        self.client.login(username=self.username, password=self.password)

class ValidateRegisterFormViewTest(UserLoginMixin, TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass() # spusti UserLoginMixin.setUpClass()
        # cls.url = reverse('accounts:validate_username') # odkazuje na ValidateUsernameView
        cls.url = reverse('accounts:validate_register_form')


    def test_returns_false_if_exists(self):
        data = {
            'field_name': 'username',
            'field_value': self.username
        }

        response = self.client.post(self.url, data=data)

        expected_json = {
            'exists': True,
            'error_message': 'The {} {} already exists'.format(*("username", self.username))
        }
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, expected_json)


    def test_returns_true_and_no_error_if_not_exist(self):
        data = {
            'field_name': 'username',
            'field_value': 'NONEXIST'
        }

        response = self.client.post(self.url, data=data)

        expected_json = {
            'exists': False,
        }

        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, expected_json)
