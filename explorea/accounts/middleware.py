import os

from django.conf import settings
from django.shortcuts import redirect

class TestMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        print('process_view')

    def process_exception(self, request, exception):
        print('process_exception')

    def process_template_response(self, request, response):
        print('process_template_response')
        return response

class LoginMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request): # tato metoda se zavola, pokud je volana instance dane class, napr. lm = LoginMiddleware(None) --> lm()
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):

        is_exempt = any(url.match(request.path_info) for url in settings.LOGIN_EXEMPT_URLS)  # re.match(pattern, string), NEBOLI pattern.match(string), kde pattern = objekt _sre.SRE_Pattern, ktery vznikne pri re.compile(r'^...$') vyhleda vyskyt NA ZACATKU stringu
                                                                                            # request.path_info vraci cestu k requestovane page; neobsahuje domenu, narozdil od .path ani tzv. script prefix
                                                                                            # do any() vstupuje generator expression: for i in .. yield .., tzn. PRVNI VYSKYT True generator ZASTAVI a nemusi byt overeny vsechny polozky seznamu URL adres
        if is_exempt or request.user.is_authenticated: # pokud se v seznamu settings.LOGIN_EXEMPT_URLS nachazi requestovana URL, NEBO user je logged-in
            return None
        else:
            return redirect(os.path.join(settings.LOGIN_URL, '?next=%s' % request.path)) # v tomto pripade se spusti LoginView (django.contrib.auth.views), ktere definuje atribut redirect_field_name = REDIRECT_FIELD_NAME
                                                                                        # v django.contrib.auth.__init__ je REDIRECT_FIELD_NAME="next"
                                                                                        # pokud je view spusteno pres POST a zalogovani usera je uspesne, redirectuje na url, ktera je specifikovana v redirect_field_name (defaultne v next),
                                                                                        # pokud v next neni URL specifikovana, redirectuje na settings.LOGIN_REDIRECT_URL (defaultne /accounts/profile/), pokud zalogovani neni uspesne, zobrazi znovu login form

                                                                                        # pro LoginView lze hodnotu next specifikovat v template login.html (pri zobrazeni login page) takto:
                                                                                        # <form method="post">
                                                                                        # ...
                                                                                        #     <input type="hidden" name="next" value="{{ next }}">
                                                                                        # </form>
