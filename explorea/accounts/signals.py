from .models import Profile

def create_profile(sender, instance, created, **kwargs): #sender je User model, ktery vola metodu save(); instance je user objekt, pro ktery se ma vytvorit profil; created - True/False (zda se podarilo usera ulozit)
    if created: # pokud novy user objekt byl ulozen do db, vytvori mu profil (do user fieldu ulozi tento novy user objekt)
        Profile.objects.create(user=instance)
    instance.profile.save() # at uz byl nebo nebyl vytvoren novy user (napr. byl updatovan), ulozi jeho profil do db
