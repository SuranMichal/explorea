from django.urls import path

from . import views

app_name = 'posts'

urlpatterns = [
    path('<slug:slug>/add_rating/', views.AddRatingView.as_view(), name='add_rating'),
    path('<slug:slug>/ratings/', views.RatingListView.as_view(), name='ratings_listing'),
]
