from django.db import models
from explorea.events.models import Event, EventRun

class Rating(models.Model):

    MARKS = ((1, 1), (2, 2), (3, 3), (4, 4), (5, 5)) # jak vytvorit vyber, pokud by bylo moznosti vice ??

    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=True)
    eventrun = models.ForeignKey(EventRun, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    mark = models.IntegerField(choices = MARKS, default = 1,)
    comment = models.TextField(max_length=1000)
