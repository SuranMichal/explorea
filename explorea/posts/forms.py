from django import forms

from explorea.events.models import EventRun
from . models import Rating

from django.forms import ModelChoiceField

class MyModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return str(obj.date) + " " + str(obj.time)

class AddRatingForm(forms.ModelForm):

    # implement MyModelChoiceField
    eventrun = MyModelChoiceField(queryset = EventRun.objects.all(), empty_label=None)

    class Meta:
        model = Rating
        exclude = ["event"]
