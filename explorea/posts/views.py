from django.shortcuts import redirect

from django.views.generic import ListView
from django.views.generic.edit import CreateView

from django.db.models import Avg, Count
from django.contrib.auth.mixins import UserPassesTestMixin

from . forms import AddRatingForm
from . models import Rating
from explorea.events.models import Event, EventRun

from explorea.events.mixins import GroupRequiredMixin

class AddRatingView (GroupRequiredMixin, CreateView):

    model = Rating
    form_class = AddRatingForm
    template_name = 'posts/rating.html'

    groups_required = ['hosts']

    def get_context_data(self, **kwargs):

        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
            kwargs['form'].fields['eventrun'].queryset = EventRun.objects.filter(event__slug=self.kwargs["slug"]).filter_passed_runs()

        return super().get_context_data(**kwargs)

    def form_valid(self, form):

        event = Event.objects.get(slug=self.kwargs["slug"])

        self.object = Rating.objects.create(event=event, **form.cleaned_data)

        return redirect(self.object.event.get_absolute_url())

class RatingListView (ListView):
    context_object_name = 'ratings'
    template_name = 'posts/ratings_listing.html'

    def get_queryset(self):
        return Rating.objects.filter(event__slug=self.kwargs["slug"])
