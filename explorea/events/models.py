from django.db import models
from django.db.models import Manager
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import Max, Q
from django.utils.text import slugify

from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

from datetime import datetime

from django.urls import reverse

def get_related_attr(obj, attrs):

    related_obj = obj # obj..event run objekt
    for attr in attrs: # v pripade sort_by = "host", tj. 'event__host__username': attrs = ["event", "host", "username"]
        related_obj = getattr(related_obj, attr) # v tomto pripade se spusti: getattr (event run objekt, event) coz vyhodi event objekt, dale getattr (event objekt, host) coz vyhodi user objekt,
                                                # a nakonec getattr (user objekt, username) coz vyhodi string napr. "Michal"
    return related_obj # vraci vzdy hodnotu urciteho fieldu, tj. string, int, apod.

def thumbnail_image_url(instance, filename): # upload_to musi mit vzdy 2 argumenty: 1) instance modelu ve kterem je FileField definovan, konkretne ta, ktera obsahuje dany soubor; 2) originalni nazev souboru (definovany userem)
    '''upload_to for Event thumbnail image'''
    return 'user_{0}/thumb_{1}'.format(instance.host.id, filename)

def main_image_url(instance, filename):
    '''upload_to for Event main image'''
    return 'user_{0}/main_{1}'.format(instance.host.id, filename)

def album_image_url(instance, filename):
    '''upload_to for Image model'''
    return 'user_{0}/{1}/{2}'.format(instance.album.event.host.id,
        instance.album.title, filename) # priklad: user_1/album_Running/photo1

class EventManager(models.Manager):

    def get_queryset(self): #timto prepiseme defaultni metodu Manageru, ktera vracela Queryset, ktery nema nadefinovane metody filter_by_category a filter_available
        return EventQuerySet(self.model, using=self._db)

    def search(self, query=None):
        lookup = ( # lookup je opet Q objekt, | znaci nebo, __icontains je case-insensitive test zda query obsazena v danych fieldech modelu Event
            Q(name__icontains=query) |
            Q(description__icontains=query) |
            Q(location__icontains=query)
        )
        return self.filter(lookup).distinct() # distinct eliminuje duplikovane vysledky vyhledavani

class EventRunManager(models.Manager):

    def get_queryset(self):
        return EventRunQuerySet(self.model, using=self._db)

    def search(self, query=None):
        lookup = (
            Q(event__name__icontains=query) |
            Q(event__description__icontains=query) |
            Q(event__location__icontains=query)
        )
        return self.filter(lookup).distinct()

class EventQuerySet(models.QuerySet): #timto custom QuerySetu nadefinujeme custom metody
#tim padem ale nelze uz pouzit EventManager metodu napr. Event.objects.filter_by_category('fun'), ale musime pridat all(),
#tj. Event.objects.all().filter_by_category('fun'), ktera vraci QUERYSET objekt

    def filter_by_category(self, category=None):
        db_equivalent = ''
        for pair in self.model.CATEGORY_CHOICES: #po zadani objects = EventManager() uvnitr class Event --> self.model =  Event
            if pair[1] == category:
                db_equivalent = pair[0]
                break
        else:
            return self.all()

        return self.filter(category=db_equivalent) #vraci EventQuerySet eventu, spadajicich do userem zvolene kategorie

    def filter_available(self, date_from=None, date_to=None, guests=None):# filtruje dostupne eventy v uzivatelem zadanem casovem rozmezi a s uzivatelem zadanym minimalnim poctem volnych mist
        date_from = date_from or timezone.now().date()# pokud user nevybere date from, zobrazi se vsechny eventy odted..
        #qs = self.annotate(max_seats=Max('eventrun__seats_available'))# tzv. anotace - pro kazdy objekt tridy Event (pro kazdy event) vytvori atribut max_seats,
            #  kam ulozi maximalni hodnotu atributu seats_available ze vsech event runu (objektu tridy EventRun) daneho eventu (musi byt spojeni pres ForeignKey)
            # 'eventrun__seats_available' - atribut seats_available modelu EventRun, eventrun__seats_available__max - cely nazev anotovaneho atributu objektu Event s nazvem max_seats
            # Event.objects.get(pk=5).eventrun_set.aggregate(Max('seats_available')) vrati {'seats_available__max': 2} --> agregace

        if date_to: # pokud user nevybere date to, neni limit dokdy..
            qs = EventRun.objects.filter(date__range=(date_from, date_to))# __range je atribut DateField
        else:
            qs = EventRun.objects.filter(date__gte=date_from)

        if guests:
            qs = qs.filter(seats_available__gte=guests)

        return self.filter(pk__in=[qs.values_list('event', flat=True)])     #vraci EventQuerySet tech eventu, ktere maji event runy v userem vybranem datovem rozpeti s jim vybranym min. poctem mist


        # values()...misto <QuerySet [<EventRun: EventRun object (1)>,...]> varci <QuerySet [{'id': 16, 'event_id': 2, 'date': ..., ...}, {...}]>, tedy kazdy objekt je slovnik a key:value = atributy/sloupce v databazi:jejich hodnoty
        # values_list()...vraci <QuerySet [(16, 2, datetime.date(2018, 12, 19),...), (...)]>, tedy kazdy objekt jako tuple, obsahujici jen values
        # qs.values_list('event')... vraci <QuerySet [(2,), (18,)]>, tedy tuple obsahuje jen values sloupce event
        # qs.values_list('event', flat=True)... vraci <QuerySet [2, 18]>

        # pk__in filtruje ty objekty, jejichz private key = [...], v [] muze byt jak string, tak napr. <QuerySet [2, 18]>, tzn. return vraci objekt (pk je jedinecny)

class EventRunQuerySet(models.QuerySet):

    def filter_by_category(self, category=None):
        db_equivalent = ''
        for pair in Event.CATEGORY_CHOICES:
            if pair[1] == category:
                db_equivalent = pair[0]
                break
        else:
            return self.all()

        return self.filter(event__category=db_equivalent) # vraci EventRunQuerySet vsech event runu v db tech eventu, ktere spadaji do zvolene kategorie

    def filter_available(self, date_from=None, date_to=None, guests=None):

        date_from = date_from or timezone.now().date()

        if date_to:
            qs = self.filter(date__range=(date_from, date_to))
        else:
            qs = self.filter(date__gte=date_from)

        if guests:
            qs = qs.filter(seats_available__gte=guests)

        return qs # qs - vraci EventRunQuerySet tech event runu vsech eventu v db, ktere spadaji do userem zadaneho datoveho rozmezi s min. poctem mist

    # def filter_first_available(self, date_from=None, date_to=None, guests=None, sort_by="date"): # pro DB SQLite3
    #     qs = self.filter_available(date_from, date_to, guests).order_by('date', 'time') # vraci qs, kde jsou event runy serazene podle data, potom podle casu vzestupne
    #     event_ids = []
    #     filtered=[]
    #     for run in qs:
    #         if not run.event.id in event_ids: # pripoji vzdy prvni objekt event run (s nejdrivejsim datem, casem) urciteho eventu do listu filtered
    #             filtered.append(run)
    #             event_ids.append(run.event.id)
    #
    #     #return filtered # vraci list event run objektu (pro kazdy event vzdy jeden - ten prvni)

    def filter_first_available(self, date_from=None, date_to=None, guests=None, sort_by='date'): # pro DB postgreSQL
        qs = self.filter_available(date_from, date_to, guests)
        qs = qs.order_by('event_id', 'date', 'time').distinct('event_id') # fieldy v distinct() - lze definovat pouze v postgreSQL; musi byt shodne a ve stejnem poradi jako v order_by();
        # return qs # ze serazeneho qs se vyberou pouze prvni objekty pro kazde event_id (tj. s nejdrivejsim datem a casem)

        reverse, fields = (sort_by.startswith('-'), sort_by.lstrip('-').split('__')) # reverse = True/False, fields = ["attr", ...]

        criterion = lambda obj: get_related_attr(obj, fields) # definice funkce criterion # criterion ziska pro kazdy objekt z listu filtered hodnotu fieldu (attr), podle ktere pak objekty srovna funkce sorted ??

        resulta =  sorted(qs, key= criterion, reverse=reverse) # sorted - prvni arg je iterable, druhy nazev funkce (napr. len), treti reverse=True/False
        return resulta # vysledkem je serazeny list event run objektu (tech nejdrivejsich pro kazdy event) podle hodnoty daneho attr (price, host...)

    def filter_active_runs (self):

        return self.filter(date__gte=timezone.now().date())

    def filter_passed_runs (self):

        return self.filter(date__lte=timezone.now().date())

class Event(models.Model):

    FUN = 'FN'
    RELAX = 'RX'
    EXP = 'EX'
    SIGHTS = 'SI'

    CATEGORY_CHOICES= ( # prvni polozka tuple - uložena do DB, druha - zobrazena userovi ve formu
        (FUN, 'fun'),
        (RELAX, 'relax'),
        (EXP, 'experience'),
        (SIGHTS, 'sights')
    )

    host=models.ForeignKey(User,
        on_delete=models.CASCADE, null=True, default=None)
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True, null=True)# unique=True..tento column musi obsahovat unikatni values v ramci db tabulky;
                                                        # null=True.. musime zadat, jinak bude problem pri migraci (vytvoril by se novy prazdny sloupec, coz by nevyhovelo null=False --> konflikt)
    location = models.CharField(max_length=500)
    description = models.TextField(max_length=1000)
    created = models.DateTimeField(auto_now_add=True, null=True) #auto_now_add=True...automaticky pri vytvoreni objektu se nastavi hodnota fieldu na aktualni cas, kterou pozdeji nelze zmenit

    category = models.CharField( #ve formu se objevi label Category:
        max_length=20,
        choices = CATEGORY_CHOICES,
        default = FUN, #ten, co je ve vyberu uveden jako prvni
    )
    thumbnail = ProcessedImageField(upload_to=thumbnail_image_url,
        processors=[ResizeToFill(300,200)],   # ProcessedImageField - feature package django-imagekit (ktery vyzaduje Pillow), funguje uplne stejne jako django ImageField, tzn. uklada cestu k souboru do databaze,
        format='PNG',                           #  jeste existuje ImageSpecField - nepracuje s db # ResizeToFill - v zavorkach pozadovana velikost image
        options={'quality':60},
        null=True,
        blank=True)
    main_image = ProcessedImageField(upload_to=main_image_url,
        processors=[ResizeToFill(500,600)],
        format='PNG',
        options={'quality':100},
        null=True, # pri ukladani do db je u daneho fieldu povolena hodnota null
        blank=True) # ve formu, ktery updatuje dane field, nebude field required

    objects = EventManager() # timto se prepise atribut objects (pro model Event) s defaultnimi metodami (all, filter..)
                            # na objects s nami definovanou metodou events_by_category

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs): # custom save metoda, ktera prepise tu puvodni, definovanou pro class Model # *args, **kwargs - zaruci, ze pokud ze strany Djanga budou pridany argumenty built-in funkce, bude je custom funkce automaticky podporovat
        # if not self.slug: # pri vytvoreni noveho eventu (kdy se vola metoda save) automaticky vytvori column slug (pokud neexistuje) a zapise do neho value, dale pokracuje stejne jako parent metoda save # kdyz chceme zmenit name eventu, slug se nezmeni ! --> odstraneno
        self.slug = slugify(self.name + '-with-' + self.host.username)
        super().save(*args, **kwargs)
        if not hasattr(self, 'album'): # kdyz se vytvori novy event, vytvori se k nemu automaticky album, alternativni pristup k SIGNALS
            Album.objects.create(event=self)

    def get_absolute_url(self):
        return reverse('events:event_detail', args=[self.slug]) # prvni arg je nazev views nebo name URL patternu v urls.py/path; vrati cestu: do 'detail/<slug:slug>/' dosadi neco-with-nekdo, tzn. 'detail/neco-with-nekdo/'

    class Meta:
        ordering = ['name']#v pripade zobrazeni radi objekty modelu od A do Z podle atributu name
        unique_together = (("name", "host"),)#tuple of tuples, nebo jen tuple, kombinace fieldu v tuple musi byt unikatni; jinak raisuje ValidationError
        verbose_name='event' # nazev, ktery se pro dany model bude zobrazovat v Django Admin

class EventRun(models.Model):

    event = models.ForeignKey(Event,
        on_delete=models.CASCADE)
    date = models.DateField(blank=False, null=False)
    time = models.TimeField(blank=False, null=False)
    seats_available = models.PositiveIntegerField(blank=False, null=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, blank=False, null=False)

    objects = EventRunManager()

    def __str__(self):
        return '{}|{} ({})'.format(self.date, self.event, self.event.category)

    class Meta:
        ordering = ['date', 'time']# radi objekty nejprve podle date, potom podle time, oboji od drive do pozdeji (vzestupne)

class Album(models.Model):
    event = models.OneToOneField(Event, on_delete=models.CASCADE) # vztah 1:1
    title = models.CharField(max_length=200, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs): # custom save --> pri volani save(), tzn. kdyz je eventu prirazeno album, ktere nema title, ulozi album pod nazvem album_<event.name>, pro zbytek procesu se vola parent save()
        if not self.title:
            self.title = "album_" + self.event.name
        super().save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.title)

class Image(models.Model):
    album = models.ForeignKey(Album, on_delete=models.CASCADE) # vztah n:1
    image = ProcessedImageField(upload_to=album_image_url,
            processors=[ResizeToFill(500,400)],
            format='PNG',
            options={'quality':80})
    title = models.CharField(max_length=200, null=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}/{}'.format(self.album, self.title)


    class Meta:
        ordering = ['created'] # seradi Image instances podle data vytvoreni vzestupne
