from django import forms
from config.core.validators import validate_human_name #, validate_date
from django.core.exceptions import ValidationError
from django.utils import timezone

from .models import Event, EventRun

from django.forms import ModelChoiceField

class MyModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return str(obj.date) + " " + str(obj.time)

class EventForm(forms.ModelForm):

    name = forms.CharField(max_length=100, validators=[validate_human_name,])
    location = forms.CharField(max_length=100, validators=[validate_human_name,])
    gallery = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=False)

    class Meta:
        model = Event
        exclude = ["host", "slug"]

#      # v Event modelu jiz definovan atribut category, tento by ho prepsal
#     CHOICES = (('1', 'Fun',), ('2', 'Relax',),('3', 'Adventure',),('4', 'Other',))
#     category = forms.ChoiceField(
#         label="Event category",
#         widget=forms.Select, choices=CHOICES,
#     )


class EventRunForm(forms.ModelForm):

    date = forms.DateField( #pouziva default validator, moznost nadefinovat vlastni ve validators.py
        input_formats=["%d.%m.%Y"],
        widget=forms.DateInput(format = '%d.%m.%Y'),#definuje jak bude date zobrazeno napr. pri update run(instance=...)
        help_text="Please use the following format: %d.%m.%Y",
        #validators=[validate_date,],
    )

    time = forms.TimeField( #pouziva default validator, moznost nadefinovat vlastni ve validators.py
        help_text="Please use the following format: %H:%M",
    )

    class Meta:
        model = EventRun
        exclude = ["event"]
    #   fields = "__all__"


#     event = forms.ModelChoiceField(
#         queryset=Event.objects.all(),
#         empty_label=None,
#         to_field_name="id",#melo by urcovat, ktery atribut se zobrazi v rozbalovaci liste, ale vraci object.name!!??
#     )


class EventFilterForm(forms.Form):

    PRICE_ASC='price'
    PRICE_DESC='-price'
    SEATS='-seats_available'
    DATE='date'
    NAME='event__name'
    HOST='event__host__username'

    SORT_CHOICES = (
        (PRICE_ASC, 'cheapest'),
        (PRICE_DESC, 'most expensive'),
        (SEATS,'seats available'),
        (DATE,'date'),
        (NAME,'name'),
        (HOST, 'host'),
    )

    date_from = forms.DateField(label="From", initial=None,
     widget=forms.SelectDateWidget, required=False)

    date_to = forms.DateField(label="To", initial=None,
        widget=forms.SelectDateWidget, required=False,)

    guests = forms.IntegerField(required=False, min_value=1)

    sort_by = forms.ChoiceField(label="Sort by", choices=SORT_CHOICES,
            required=False)

    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.initial['sort_by'] = 'date'

    def clean (self):
        cleaned_data = super().clean()
        date_from= cleaned_data.get ("date_from")
        date_to= cleaned_data.get ("date_to")

        msg1 = 'Date FROM later than date TO'
        msg2 = 'Selected date in the past'

        if ((date_from and date_to) and date_from > date_to):
            self.add_error('date_from', msg1)

        for name, date in [('date_from', date_from), ('date_to', date_to)]:
            if date and date < timezone.now().date():
                self.add_error(name, msg2) # self - form instance

    #   self.errors --> {'date_from': ['Date FROM later than date TO', 'Selected date in the past'], 'date_to': ['Selected date in the past']}

        if not self.cleaned_data.get('sort_by'):
            self.cleaned_data['sort_by'] = 'date'
        self.cleaned_data


#         if not date_from <= date_to:
#             raise ValidationError(msg1, code='invalid') # po spusteni tohoto radku, pokud ve formulari zadame obracene datum: self.non_field_errors() = ['Date FROM later than date TO'],
#                                                         # self.errors= {"__all__":["date FROM later than date TO"]} # self - form instance
#         error_dict = {}
#         if not date_from <= date_to:
#              error_dict["date_from"] = ValidationError(msg1, code='invalid')
#         if not all(date >= timezone.now().date() for date in (date_from,date_to)):
#              error_dict["date_to"] = ValidationError(msg2, code='invalid')
#         if error_dict:
#              raise ValidationError(error_dict)


class EventSearchFilterForm(EventFilterForm):

     q = forms.CharField(required=False, max_length=1000, initial='', widget=forms.HiddenInput())


# class MultipleFileForm(forms.Form):
#     gallery = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=False) # attrs={'multiple': True}...chceme-li uploadovat vice souboru pouzitim jednoho form fieldu
#                                                                                                         # Field(widget=...) --> HTML <form>  <element><input...></element>   </form>
