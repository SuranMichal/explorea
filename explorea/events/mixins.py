from django.core.exceptions import PermissionDenied
from django.views.generic.edit import FormMixin

from django.contrib import messages

from . models import Event, EventRun

class GroupRequiredMixin:

    groups_required = []

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated: # pokud vstupuje AnonymousUser, vyhodi HTTP 403 Forbidden stranku
            raise PermissionDenied
        else:
            user_groups = set(request.user.groups.values_list('name', flat=True)) # user.groups.all() ... queryset objektu, napr. <Group: hosts>
                                                                                # user.groups.values_list('name') ... queryset tuplu, napr. ('hosts',)
                                                                                # user.groups.values_list('name', flat=True) ... qs stringu, napr. 'hosts'
                                                                                # user_groups vrati {'hosts'}

            if not user_groups.intersection(set(self.groups_required)):         # A.intersection(B) ... vrati novy set s elementy spolecnymi pro set A a B
                                                                                # pokud neexistuje spolecna group pro: 1) groups, jejichz clenem je user a
                                                                                #                                    2) groups_required,
                                                                                # vyhodi HTTP 403 Forbidden stranku
                raise PermissionDenied
        return super().dispatch(request, *args, **kwargs) # spousti dispatch() pro dane view

class GetFormMixin(FormMixin):

    model = EventRun
    context_object_name = 'event_runs'
    template_name = 'events/events_listing.html'
    paginate_by = 4

    def get_form_kwargs(self): # overridujeme, protoze FormMixin.get_form_kwargs() do kwargs uklada jen z request.POST nebo request.FILES
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
            'data': self.request.GET,
        }
        return kwargs

    def get_context_data(self, **kwargs): # chceme, aby data, ktera user vlozil do formu, byla zapamatovana
        context = super().get_context_data(**kwargs)
        context['filter_form'] = self.form # form se ulozi do context pod "filter_form"
        return context

class MessageActionMixin:

    @property
    def success_message(self):
        return NotImplemented

    @property
    def error_message(self):
        return NotImplemented

    def get_object(self, queryset=None):
        return getattr(self, 'object', None) or super().get_object(queryset) # self = <..views.UpdateEventView object at..>, getattr(self, 'object', None), coz je self.object (atribut je jiz vytvoreny ), vyhodi napr. <Event: Waiting in Zabreh>

    def form_valid(self, form):

        obj = self.get_object()

        if isinstance (obj, Event):
            messages.success(self.request, self.success_message % obj.__dict__) # ulozi do requestu messages # hleda u classes, ktere dedi z MessageActionMixin, atribut success_message
                                                                                # for i in self.request._messages: print (i) --> The event Waiting in Zabreh has been updated successfully
        elif isinstance (obj, EventRun):
            messages.success(self.request, self.success_message % obj.event.__dict__)

        # return super().form_valid(form)

    # nasleduje genericke reseni (Martin - engeto) - upraveno (v puvodni verzi v pripade updaterun vsak nezna name, v pripade updateevent vraci None)
    def form_invalid (self, form):
        try:
            obj = self.get_object()
        except: # jakakoliv exception (nepristoupi k objektu)
            messages.error(self.request, self.error_message) # v pripade, ze nepristoupi k objektu, vraci jen obecne error message
            return super().form_invalid(form=form)
        else: # spusti se pokud nebyly raisovany zadne errory (pristoupi k objektu)
            try:
                messages.error(self.request, self.error_message % obj.__dict__)
            except:
                messages.error(self.request, self.error_message % obj.event.__dict__)
            finally:
                return super().form_invalid(form=form)


    def delete(self, request, *args, **kwargs):
        obj = self.get_object()
        if isinstance (obj, Event):
            messages.success(self.request, self.success_message % obj.__dict__)
        elif isinstance (obj, EventRun):
            messages.success(self.request, self.success_message % obj.event.__dict__)
        return super().delete(request, *args, **kwargs)

""" # toto reseni neni genericke, je konkretni
class CreateMessageActionMixin (MessageActionMixin):

    def form_invalid(self, form):
        if self.model == EventRun:
            obj = Event.objects.get(slug=self.kwargs["slug"])
            messages.error(self.request, self.error_message % obj.__dict__)
        elif self.model == Event:
            obj = self.request.POST
            messages.error(self.request, self.error_message % obj) # obj je QueryDict
        return super().form_invalid(form)

class UpdateDeleteMessageActionMixin (MessageActionMixin):

    def form_invalid(self, form):
        obj = self.get_object()
        if isinstance (obj, Event):
            messages.error(self.request, self.error_message % obj.__dict__)
        elif isinstance (obj, EventRun):
            messages.error(self.request, self.error_message % obj.event.__dict__)
        return super().form_invalid(form)
"""
