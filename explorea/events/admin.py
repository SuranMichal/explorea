from django.contrib import admin

from django.db.models import Count, Q
from django.utils import timezone
from django.http import HttpResponse
from django.urls import path, reverse
from django.shortcuts import render, redirect
from django import forms
from django.utils.html import format_html

from django.contrib.auth import get_user_model
from django.contrib import messages

from .models import Event, EventRun

import csv

from io import StringIO

UserModel = get_user_model()

admin.site.site_header = 'Explorea Admin' # nazev na hlavni strance
admin.site.site_title = 'Explorea Admin' # nazev v browser tab
admin.site.index_title = 'Select the model ...'# podnazev na hlavni strance

# Register your models here.

def export_to_csv(modeladmin, request, queryset): # queryset je zaskrtnuty event, eventy; modeladmin je instance EventAdmin
    meta = modeladmin.model._meta # modeladmin.model = <class 'explorea.events.models.Event'>, modeladmin.model._meta = <Options for Event>...Options objekt

    # get table header
    field_names = [field.name for field in meta.fields] # meta.fields (funkce tridy django.db.models.options.Options) vyhodi vsechny fieldy na modelu Event
    filename = '{}_Events'.format(timezone.now().strftime('%Y_%m_%d')) # napr. '2019_01_17_Events'

    # create response object to be sent
    response = HttpResponse(content_type='text/csv') # vytvori prazdny HttpResponse objekt, ktery ma vracet csv format  (content_type definuje Multipurpose Internet Mail Extensions (MIME) typ)
    response['Content-Disposition'] = 'attachment; filename={}.csv'.format(filename) # Content-Disposition: inline - zobrazeni jako text web stranky, attachment - jako soubor ke stazeni na web strance,
                                                                                    # filename = parametr attachment, defautni nazev, ktery se zobrazeni v dialogu Save As

    # csv writer object that needs the response to write in
    writer = csv.writer(response) # response je file-like objekt

    # write the data
    writer.writerow(field_names) # field_names = ['id', 'host', 'name', 'slug', 'location', 'description', 'created', 'category', 'thumbnail', 'main_image'] - v csv souboru vytvori radek s temito stringy oddelenymi oddelovacem
    for obj in queryset: # pro kazdy objekt z qs:
        writer.writerow(getattr(obj, field) for field in field_names)  # pro kazdy field ziska jeho hodnotu a zapise na radek

    return response

class CSVImportForm(forms.Form):
    file = forms.FileField()

class EventRunInline(admin.TabularInline):
    model = EventRun

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['host', # zobrazi fieldy tabulky modelu Event jako sloupce na strance "Select Event to change", poradi polozek listu definuje poradi zobrazeni
                    'name',
                    'category',
                    'events_passed',
                    'events_active',
                    'slug',
                    'truncate_description',
                    'location',
                    'created',
                    'thumbnail',
                    'main_image',]

    list_filter = ['category', 'created'] # v pravem panelu se objevi moznosti filtrovat tabulku podle zadanych atributu
    date_hierarchy = 'created' # nad tabulkou (nad Action) rozvinovaci link, kde lze vybrat pouze udalosti vytvorene v konkretni datum
    search_fields = ['host__username', 'name', 'location'] # nad tabulkou se vytvori vyhledavaci pole, podle zadanych fieldu pujde vyhledavat
    list_editable = ['name', 'category', 'location'] # seznam tech sloupcu, ktere pujde editovat primo v zakladni tabulce
    actions = [export_to_csv] # actions bude funkce, proto nazev promenne  misto string (funkce - lze pouzit i mimo class EventAdmin)
    change_list_template = "admin/events_changelist.html" # nazev custom template
    readonly_fields = ['thumbnail_image', 'main_image_picture'] # tyto fields nepujde menit v Django Admin (tyto nejsou na modelu, ale jsou pridane metodou v Django Admin)
    inlines = [EventRunInline] # po rozkliknuti daneho eventu se objevi k updatu krome fieldu Eventu i fieldy jeho EventRunu

    def get_queryset(self, request): # override BaseModelAdmin.get_queryset()
        qs = super().get_queryset(request)
        qs = qs.annotate( #count() vs annotate(): count() - pokazde kdyz chceme zobrazit tabulku provede db query pro kazdy radek, annotate () - provede to smae jen pri prvnim zobrazeni tabulky
            events_passed=Count('eventrun', filter=Q(eventrun__date__lt=timezone.now())), # ke kazde polozce z qs (radku tabulky) pripoji field (sloupec) events_passed, a ulozi do nej pocet eventrunu s nizsim datem nez je nyni # qs[x].events_passed vrati jejich pocet pro dany event
            events_future=Count('eventrun', filter=Q(eventrun__date__gte=timezone.now()))
        )
        return qs

    def events_passed(self, obj): # musi byt 2 parametry: 1)self, 2)reference na model instance (obj)
        return obj.events_passed  # nyni se muzeme na metody odkazovat v list_display
    events_passed.admin_order_field = 'events_passed' # vypocitana pole defaultne nejdou radit, timto razeni implementujeme; string = anotovane pole v qs
                                                    # definovane jako atribut class EventAdmin !
    def events_active(self, obj):
        return obj.events_future
    events_active.admin_order_field = 'events_future'

    def truncate_description(self, obj): # za kazdym textem zobrazi..., pokud bude delka textu ve fieldu Description >40, usekne zbytek
                                        # aby se sloupec pridal, musi se pridat do list_display
        length = 40
        if len(obj.description) > length:
            return obj.description[:length] + '...'
        elif len(obj.description) > 0:
            return obj.description + '...'
        else:
            return '-----------'
    truncate_description.short_description = "Description" # nazev sloupce bude Description, jinak by to bylo Truncate Description

    def get_urls(self): # override ModelAdmin.get_urls() - pridavame vlastni urlpattern
        import_url = [path('import_csv/', self.import_csv, name='import_csv')]
        return import_url + super().get_urls() # custom pattern musi byt prvni, jinak bude django zkouset vyhledat event pouzitim id ??

    def import_csv(self, request):
        if request.method == 'POST':

            form = CSVImportForm(files=request.FILES)
            if form.is_valid():

                file = request.FILES['file'] # <InMemoryUploadedFile: 2019_01_17_Events_u.csv (application/vnd.ms-excel)>
                data_stream = StringIO() # vytvori se prazdny memory buffer
                for chunk in file.chunks(): # lepsi nez File.read() - v pripade velkeho file se zahlti memory (default velikost chunk=64kB); v tomto pripade se stejne nacte soubor naraz
                    data_stream.write(chunk.decode()) # file je v bytes, proto se nejprve musi dekodovat na string; pote se zapise do bufferu

                try:
                    data_stream.seek(0)
                    reader = csv.DictReader(data_stream)

                    #for line in reader: # kazda line je jakoby jeden budouci Event objekt
                    for index, line in enumerate(reader): # index je cislo radku, line je OrderedDict (radek)
                        line['host'] = UserModel.objects.get(pk=int(line['host'])) # host column v importovanem csv musi obsahovat user.id !! oddelovac seznamu musi byt carka !!
                        Event.objects.update_or_create(**line) # line je napr. OrderedDict([('id', '10'), ('host', 'Michal'), ('name', 'Cooking'), ..., ('main_image', 'user_2/main_Tulips.png')]),
                                                        # pokud na modelu Event existuje
                    self.message_user(request, "The file {} has been imported".format(file.name))

                except Exception as e: # e = ValueError("invalid literal for int() with base 10: 'j'",)

                    messages.error(request,
                          ("The file {} could not be imported. "
                          "Problem on line {}. "
                          "(HINT: {})").format(file.name, index + 2, e.args[0])) # e.args = ("invalid literal for int() with base 10: 'j'",)
                finally:
                    data_stream.close()

                return redirect(reverse('admin:events_event_changelist')) # '/admin/events/event/'

        form = CSVImportForm()
        return render(request, 'admin/import_csv.html', {'form': form})

    def thumbnail_image(self, obj):
        return format_html('<img src={}>', obj.thumbnail.url) # format_html(): prosty text je escapovan (dekodovan) pro pouziti v html

    def main_image_picture(self, obj):
        return format_html('<img src={}>', obj.main_image.url)
