from django import template #importuje package template, ktery ma v __init__.py: from .library import Library --> mame pristup k template.Library
from django.utils import timezone


register = template.Library()#objekt template.Library ma pristup k metodam Library - napr. filter, simple_tag...

@register.filter
def has(obj, attr_name):#popis viz accounts_tags.py
    try:
        return bool(getattr(obj, attr_name))
    except ValueError:
        return False

@register.filter# min_attr_value=register.filter(min_attr_value)
def min_attr_value(objects, field_name):# objects jsou eventruny daneho eventu, field_name je vybrany atribut eventrun (price,date),
                                        # za field_name se dosadi value definovana v event_listing.html, v tomto pripade price {{ event.eventrun_set.all|active|min_attr_value:"price" }}
    values = [getattr(obj,field_name) for obj in objects]#pro kazdy eventrun ziska hodnotu vybraneho atributu a vytvori list s nazvem values
    if values:
        return min(values)

@register.filter
def max_attr_value(objects, field_name):
    values = [getattr(obj,field_name) for obj in objects]
    if values:
        return max(values)

"""
@register.filter
def next_date(objects,field_name="date"):
    dates = sorted(objects.values_list(field_name, flat=True)) # objects.values_list(field_name, flat=True) vraci napr. <QuerySet[neco, neco]>, sorted to konvertuje na list [neco, neco]
    for dt in dates:
        if dt >= timezone.now().date():
            return dt
"""

@register.filter
def active(objects, date_field="date"):
    return [obj for obj in objects if getattr(obj,date_field) >= timezone.now().date()]
