import re
from django import template

register = template.Library()

@register.simple_tag(takes_context=True)#funkce se timto dostane k promennym templatu, napr. request
def next_page(context, page_num):# page_num je stranka, kterou si user preje videt

    request = context.get('request')# context je list slovniku - lze se k nemu dostat pres pdb tag v templatu - {% pdb %} vlozit na konec kodu v pagination.html
    path = request.get_full_path() # request.get_full_path je string napr. "/events/?date_from_month=1&...&guests="
    regex = re.compile(r'page=\d*\&?')  # r' nebere v potaz zvlastni python znaky (napr. "\n"), \d*\&? znamena 0 nebo vice znaku [0-9] nasledovanych 0-1 znakem &
    URL = regex.sub('', path).rstrip('&') # nahradi pattern napr. "page=12&" v path prazdnym stringem a usekne "&" pred page

    if '?' not in URL:
        URL += '?'
    else:
        URL += '&'

    URL += 'page={}'.format(page_num) # do URL vlozi napr. "page=13"

    return URL
