# Generated by Django 2.1.2 on 2019-01-02 18:47

from django.db import migrations
import explorea.events.models
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20181216_1624'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='main_image',
            field=imagekit.models.fields.ProcessedImageField(null=True, upload_to=explorea.events.models.main_image_url),
        ),
        migrations.AddField(
            model_name='event',
            name='thumbnail',
            field=imagekit.models.fields.ProcessedImageField(null=True, upload_to=explorea.events.models.thumbnail_image_url),
        ),
    ]
