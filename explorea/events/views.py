from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.core.exceptions import PermissionDenied

from django.db.models import Avg, Count

from django.views.generic import ListView, DetailView, TemplateView
from django.views.generic.edit import DeleteView, CreateView, UpdateView, FormView, FormMixin

# from django.contrib.auth.mixins import LoginRequiredMixin # LoginRequiredMixin byl z dedicnosti odstranen po zavedeni custom middleware
from django.contrib.auth.mixins import UserPassesTestMixin

from django.urls import reverse_lazy
from django.contrib import messages

from .forms import EventForm, EventRunForm, EventFilterForm, EventSearchFilterForm
from explorea.cart.forms import CartAddForm
from explorea.cart.cart import Cart

from . models import Event, EventRun, Album, Image
from explorea.posts.models import Rating

from django.utils import timezone

from .mixins import GroupRequiredMixin, GetFormMixin, MessageActionMixin

# GetFormMixin a MessageActionMixin presunuty do mixins.py

class FormListView(FormMixin, ListView):

    def get(self, request, *args, **kwargs): # overridujeme, protoze ListView.get() defaultne nepracuje s formularem
        self.form = self.get_form() # doplnime funkci FormMixin.get_form(), kterou dedi GetFormMixin, ktera inicializuje formular, data request.GET ziska z custom GetFormMixin.get_form_kwargs()

        if self.form.is_valid():
            if "q" in self.form.cleaned_data:
                self.query = self.form.cleaned_data.pop('q')
            return super().get(request, *args, **kwargs) # postara se o vylistovani objektu a pagination
        else:
            self.object_list = [] #
            return self.form_invalid(self.form) # FormMixin.form_invalid() spusti FormMixin.get_context_data(), ktera inicializovany invalidni form, ktery ma na sobe errory, ulozi do context pod "form"
                                                # MultipleObjectMixin.get_context_data() pak vraci prazdnou stranku
    def get_queryset(self, qs):
        return qs.filter_first_available(**self.form.cleaned_data)


# @login_required
# def add_event(request):
#     form = EventForm(request.POST or None, request.FILES or None)
#     #file_form = MultipleFileForm(files=request.FILES or None) # request.FILES... slovnik, kde kazda value je UploadedFile objekt, ktery ma metody napr. read(), size(), name()...; key je .. z <input type="file" name="..">
#
#     if request.method == 'POST':
#         if form.is_valid():
#
#             form.cleaned_data.pop('gallery') # do cleaned_data se dostane i "gallery", vymazeme, aby se v dalsim kroku neukladaly obrazky do instance eventu s attr gallery, protoze je chceme ulozit do instance image s attr album, image, title
#             event = Event.objects.create(**form.cleaned_data, host=request.user)
#
#             for file in request.FILES.getlist('gallery'): # save the individual images # QueryDict.getlist(key, default=None)... request.FILES.getlist('gallery') vrati [<TemporaryUploadedFile: Tulips.jpg (image/jpeg)>, <...>]
#                 Image.objects.create(album=event.album, image=file, title=file.name)
#
#             return redirect (event.get_absolute_url())
#
#     return render(request, 'events/add_event.html', {'form': form}) # ve slovniku "title": "Create New Event" na template add_event.html {{title}} zobrazí Create New Event


class AddEventView(GroupRequiredMixin, MessageActionMixin, CreateView): # pridanim GroupRequiredMixin se spusteni view omezi jen pro autentizovane a autorizovane uzivatele (viz groups_required)
    model = Event
    form_class = EventForm
    template_name = 'events/add_event.html'
    success_message = 'The event %(name)s has been created successfully' # property pouzivane MessageActionMixin metodami
    error_message = 'The event %(name)s could not be created'

    groups_required = ['hosts'] # user musi byt clenem teto skupiny (coz je skupina s definovanymi permissions), aby byl povazovan za autorizovaneho
                                # overriduje GroupRequiredMixin.groups_required
    def form_valid(self, form):

        form.cleaned_data.pop('gallery')
        event = self.object = Event.objects.create(host=self.request.user, **form.cleaned_data)

        for file in self.request.FILES.getlist('gallery'):
            Image.objects.create(album=event.album, image=file, title=file.name)

    #    messages.success(self.request, 'The event has been created successfully') # tento a nasledujici 2 zakomentovane radky v AddEventView nahrazuje MessageActionMixin a AddEventView.super().form_valid(form)

        super().form_valid(form)

        return redirect(event.get_absolute_url())

#   def form_invalid(self, form):

    #    messages.error(request, 'The event could not be created')



# def index(request):
#
#     return render(request, 'events/index.html') #nyni uz veskery html kod bude v templates


class IndexView(TemplateView): # TemplateView nedefinuje metodu k ziskani objektu, vraci jen kontext do zadane template
    template_name = 'events/index.html'


# @login_required
# def my_events_listing(request):
#
#     if not request.session.get('seen'):#pokud neni v request.session key "seen", vytvori jej
#         request.session['seen'] = {}
#
#     if not request.session['seen'].get('event_listing'):#pokud neni v request.session v key "seen" key "event listing", vytvori jej s value = 0
#         request.session['seen'].update({'event_listing': 0 })
#
#     print(request.session.items())
#     stats = request.session['seen']
#     stats['event_listing'] += 1#pokud JE v request.session v key "seen" key "event_listing", pricte 1
#
#     request.session['seen'] = stats#ulozi zmeneny "event_listing counter" do objektu request.session
#
#
#     events = Event.objects.filter(host=request.user)
#
#     return render(request, 'events/my_events_listing.html', {'events': events})


class MyEventsView(ListView):
    context_object_name = 'events' # nazev pro soubor eventu, na ktery se budeme odkazovat v template (template_name) # default nazev: object_list
    template_name = 'events/my_events_listing.html'

    def get_queryset(self): # musime overridovat ListView.get_queryset(), protoze chceme jen sve eventy, ne vsechny
        return Event.objects.filter(host_id=self.request.user.id)


# def events_listing(request, category=None):
#
#     if not request.session.get('seen'):#pokud neni v request.session key "seen", vytvori jej
#         request.session['seen'] = {}
#
#     if not request.session['seen'].get('event_listing'):#pokud neni v request.session v key "seen" key "event listing", vytvori jej s value = 0
#         request.session['seen'].update({'event_listing': 0 })
#
#     print(request.session.items())
#     stats = request.session['seen']
#     stats['event_listing'] += 1#pokud JE v request.session v key "seen" key "event_listing", pricte 1
#
#     request.session['seen'] = stats#ulozi zmeneny "event_listing counter" do objektu request.session
#
#     event_runs = EventRun.objects.all().filter_by_category(category)#filter_by_category je metoda custom QuerySetu
#     filter_form =  EventFilterForm(request.GET or None)#None vytvori prazdny form
#
#     if request.GET and filter_form.is_valid():#data poslana pres http jako jednotlive parametry URL - den, mesic, rok (html - ?d=1&m=4&...)
#                                              #se spoji do Python date objektu pri validaci
#         data = filter_form.cleaned_data
#     else:
#         data = {}
#
#     event_runs = event_runs.filter_first_available(**data)# ** rozbali values ze slovniku do tuple
#
#     paginator = Paginator(event_runs, 2) #input - prvni argument QuerySet/list/tuple, ktery se rozdeli na stranky; druhy argument pocet maximalni pocet elementu na strance
#     page = request.GET.get('page') #request.GET je QueryDict objekt, napr. <QueryDict: {'date_from_month': ['1'], ..., 'date_to_year': ['2018'], 'guests': ['']}>,
#                                     # tzn. metodou get ze slovniku, ktery koduje URL, vytahneme page, kterou chce uzivatel videt
#     event_runs = paginator.get_page(page) # event_runs je Page objekt - class Page(object_list, number, paginator), napr. event_runs.object_list vyhodi queryset event runu
#
#     return render(request, 'events/events_listing.html',
#         {'event_runs': event_runs, 'filter_form': filter_form})


class EventListView(GetFormMixin, FormListView): # oranzove buttony A NASLEDNE filtrovaci pole NEBO jen jedno z toho # poradi exekuce: EventListView-->GetFormMixin-->FormMixin-->ContextMixin-->ListView-->MultipleObjectMixin-->ContextMixin ??

    form_class = EventFilterForm

#    model = EventRun
#    context_object_name = 'event_runs'
#    template_name = 'events/events_listing.html'
#    paginate_by = 4

# na events_listing.html klikneme na button Sights - pripad form valid
# na events_listing.html vyplnime filtrovaci pole date_from > date_to a klikneme na Filter - pripad form invalid

    def get_queryset(self): # tuto funkci vola ListView.get(), nechceme .all()
        qs = self.model._default_manager.all().filter_by_category(self.kwargs['category']) # oranzove buttony # category vytahneme ze slovniku kwargs, ktery obsahuje data pro inicializaci formu
        # v tuto chvili self.kwargs = {"category":"sights"}, qs=<EventRunQuerySet [<EventRun: 2019-11-18|LeoExpress booking (SI)>, <EventRun: 2020-04-14|LeoExpress booking (SI)>]>
        return super().get_queryset(qs)


#       return qs.filter_first_available(**self.form.cleaned_data) # fitrovaci pole # do funkce se poslou data z validovaneho formu a uplatni se na jiz filtrovany qs dle category
        # return vraci [<EventRun: 2019-11-18|LeoExpress booking (SI)>], cleaned data={'date_from': None, 'date_to': None, 'guests': None, 'sort_by': 'date'}


#     def get(self, request, *args, **kwargs): # overridujeme, protoze ListView.get() defaultne nepracuje s formularem
#         self.form = self.get_form() # doplnime funkci FormMixin.get_form(), kterou dedi GetFormMixin, ktera inicializuje formular, data request.GET ziska z custom GetFormMixin.get_form_kwargs()
#         # v tuto chvili: self.form = <EventFilterForm bound=True, ..., fields=(date_from,...,sort_by)>, kwargs = {"category":"sights"}
#
#         if self.form.is_valid():
#             return super().get(request, *args, **kwargs) # postara se o vylistovani objektu a pagination
#         else:
#             self.object_list = [] #
#             # self.get_context_data(form=self.form) vraci {'paginator': <django.core.paginator.Paginator object at 0x0000000003D47358>, 'page_obj': <Page 1 of 1>, 'is_paginated': False, 'object_list': [], 'event_runs': [],
#             # 'form': <EventFilterForm bound=True, valid=False, fields=(date_from;date_to;guests;sort_by)>, 'view': <explorea.events.views.EventListView object at 0x0000000004C13470>,
#             # 'filter_form': <EventFilterForm bound=True, valid=False, fields=(date_from;date_to;guests;sort_by)>}
#             # self.form.errors vraci {'date_from': ['Selected date in the past']}
#             return self.form_invalid(self.form) # FormMixin.form_invalid() spusti FormMixin.get_context_data(), ktera inicializovany invalidni form, ktery ma na sobe errory, ulozi do context pod "form"
#                                                 # MultipleObjectMixin.get_context_data() pak vraci prazdnou stranku


#     def get_context_data(self, **kwargs): # chceme, aby data, ktera user vlozil do formu, byla zapamatovana
#         context = super().get_context_data(**kwargs)
#         context['filter_form'] = self.form # form se ulozi do context pod "filter_form"
#         return context
#         # return vraci {'paginator': <django.core.paginator.Paginator object at 0x00000000042F22E8>, 'page_obj': <Page 1 of 1>, 'is_paginated': False, 'object_list': [<EventRun: 2019-11-18|LeoExpress booking (SI)>],
#         # 'event_runs': [<EventRun: 2019-11-18|LeoExpress booking (SI)>], 'form': <EventFilterForm bound=True, valid=Unknown, fields=(date_from;date_to;guests;sort_by)>, 'view': <explorea.events.views.EventListView object at 0x0000000004099908>,
#         # 'filter_form': <EventFilterForm bound=True, valid=True, fields=(date_from;date_to;guests;sort_by)>}



# def event_search(request):
#     query = request.GET.get('q') # ze slovniku request.GET vytahneme value zadanou do vyhledavaciho pole
#     event_runs = EventRun.objects.search(query).filter_first_available() # search je custom funkce, ktera vraci QuerySet eventrunu vyhovujicich vyhledavacim kriteriim
#
#     filter_form =  EventFilterForm()
#
#     paginator = Paginator(event_runs, 2) # events_listing.html obsahuje pagination, proto pokud vysledky chceme zobrazit zde, musime to implementovat
#     page = request.GET.get('page')
#     event_runs = paginator.get_page(page)
#
#     return render(request, 'events/events_listing.html',
#         {'event_runs': event_runs, 'filter_form': filter_form})


class EventSearchView(GetFormMixin, FormListView): # vyhledavaci okno search.. (A NASLEDNE filtrovaci pole)

    form_class = EventSearchFilterForm

#    model = EventRun
#    context_object_name = 'event_runs' #MultipleObjectMixin.get_context_data() prida "event_runs" do slovniku "context"
    #extra_context = {'filter_form': EventFilterForm()} #ContextMixin.get_context_data() prida "filter_form" do slovniku "context"
#    template_name = 'events/events_listing.html'
#    paginate_by = 4 # kolik bude objektu na strance # v MultipleObjectMixin.get_context_data() se ulozi do page_size a posle se do paginate_queryset (qs,page_size), ktera vytvori paginator = Paginator(qs,page_size,orphans,allow_empty_first_page), orphans = max. pocet sirotku na posledni strance, allow empty.. = zda muze byt vysledkem query prazdna stranka

    def get_queryset(self):
        qs =  self.model._default_manager.search(self.query) # pole search..
        return super().get_queryset(qs)


#        return qs.filter_first_available(**self.form.cleaned_data) # filtrovaci pole


#     def get(self, request, *args, **kwargs):
#         self.form = self.get_form()
#
#         if self.form.is_valid():
#
#             self.query = self.form.cleaned_data.pop('q') # napr. z {"q":["coo"]} vrati "coo" a zaroven vyhodi q z cleaned data
#                                                         # v tuto chvili v self.form je "q" --> <EventFilterForm bound=True, ..., fields=(date_from,...,sort_by,q)>, ale v self.form.cleaned_data "q" NENI!
#             return super().get(request, *args, **kwargs)
#         else:
#             self.object_list = []
#             return self.form_invalid(self.form)


#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['filter_form'] = self.form
#         return context


#     def get_queryset(self): # vlastni verze
#         query = self.request.GET.get('q') #kdyz zadam do pole search.. napr. coo, request.GET vrati <QueryDict:{"q":["coo"]}>
#         return self.model._default_manager.search(query).filter_first_available()#self.model._default_manager = EventRun._default_manager = EventRunManager
#                                                                                 # musime overridovat, protoze get_queryset vraci defaultne .all()


# def event_detail(request, slug):
#
#     event = Event.objects.get(slug=slug)# musime pouzit get, abychom dostali jeden objekt, kdybychom pouzili filter, dostaneme EventQuerySet objekt, ktery nema eventrun_set
#     runs = event.eventrun_set.all().order_by('date', 'time') # vrati serazeny queryset eventrunu odpovidajicich danemu eventu - nejdriv podle data, potom podle casu - vzestupne
#     runs = runs.filter_active_runs()
#     args = {'event': event, 'runs': runs}
#
#     return render(request, 'events/event_detail.html', args)


class EventDetailView(DetailView):
    model = Event # na self.model se odkazuje get_queryset(), ktery vraci vsechny objekty modelu
    template_name = 'events/event_detail.html'
    context_object_name = 'event'
    form_class = CartAddForm # CartAddForm pouze posleme do kontextu

    def get_context_data(self, **kwargs): # musime overridovat, protoze DetailView.get_context_data() uklada jen jednu promennou (my potrebujeme 2 - event, runs)
        context = super().get_context_data(**kwargs) #DetailView.get_context_data() vraci kontextovy slovnik
        context['runs'] = self.object.eventrun_set.all().order_by('date', 'time').filter_active_runs() # do kontextoveho slovniku potrebujeme jeste pridat "runs", abychom se na nej mohli odkazovat v template
                                                                                                        # PREDELAT! v lekci pouze self.object.active_runs() a v event_detail(): runs = event.active_runs()
        context['avg_rating'] = self.object.rating_set.aggregate(avg=Avg('mark'))["avg"]
        context['num_ratings'] = self.object.rating_set.aggregate(num=Count('mark'))["num"]

        context['cart_add_form'] = self.form_class() # vyhodi <CartAddForm bound=False, valid=Unknown, fields=(quantity;product_id)>; .form_class vyhodi <class 'explorea.cart.forms.CartAddForm'>, () vytvori instanci formu
        context["cart"] = Cart(self.request.session) # ?
        return context


# @login_required
# def add_run(request, slug):
#
#     if request.method == 'POST':
#         form = EventRunForm(request.POST)
#
#         if form.is_valid():
#             event_run = form.save(commit=False)
#             event_run.event = Event.objects.get(slug=slug)
#             event_run.save()
#
#             messages.success(request, 'The run has been created successfully') # k message lze pristoupit: 1) from django.contrib.messages import get_messages, get_messages(request); 2) request._messages --> for i in request._messages: print (i)
#
#             return redirect(event_run.event.get_absolute_url())
#         else:
#             messages.error(request, 'The run could not be created')
#
#     args = {'form': EventRunForm()}
#     return render(request, 'events/add_run.html', args)


class AddEventRunView(GroupRequiredMixin, MessageActionMixin, CreateView):
    model = EventRun
    form_class = EventRunForm
    template_name = 'events/add_run.html'
#    success_message = 'The event run for event %(name)s has been created successfully' # property pouzivane MessageActionMixin metodami
    success_message = 'The event run has been created successfully'  # musi byt genericke reseni pro reseni Martin - engeto
    error_message = 'The event run for event %(name)s could not be created'

    groups_required = ['hosts']

    def form_valid(self, form):

        event = Event.objects.get(slug=self.kwargs["slug"]) # self.kwargs je to, co do views posila urls (vstupuje uz do dispatch())

        self.object = EventRun.objects.create(event=event, **form.cleaned_data) # prepiseme default self.object=form.save(), priradime event run k eventu, jehoz slug prijde z urls

        super().form_valid(form)

        return redirect(self.object.event.get_absolute_url())


# @login_required
# def update_event(request, event_id):
#
#     updated_event = Event.objects.filter(id=event_id)[0] #to, co se predvyplni ve form
#
#     if request.method == 'POST':
#
#         form = EventForm(data=request.POST)
#
#         if form.is_valid():
#
#             event = form.save(commit=False)
#             event.host = request.user
#             event.id=int(event_id)# pokud nedefinujeme, AutoField generuje ID automaticky tak, ze pricte 1!, event_id je string!
#             event.save()
#             #updated_event.delete()
#
#             return redirect('events:event_detail', event_id)
#
#         return render(request, 'events/update_event.html', {'form': form} )
#
#     form = EventForm(instance=updated_event)#predvyplneni
#     return render(request, 'events/update_event.html', {'form': form} )


# @login_required
# def update_event(request, slug):
#     event = Event.objects.get(slug=slug)
#     event_form = EventForm(request.POST or None, request.FILES or None, instance=event) # request.FILES je slovnik, kam se ukladaji uploadovane soubory (pristupne: request.FILES['file']...file je nazev fieldu ModelFormu)
#                                                                                         # request.FILES obsahuje data jen kdyz: 1) request.method == 'POST', 2) form, ktery uploaduje data je enctype="multipart/form-data"
#     file_form = MultipleFileForm(files=request.FILES or None)
#
#     if request.method == 'POST':
#
#         if event_form.is_valid() and file_form.is_valid():
#             event = event_form.save()
#
#             for file in request.FILES.getlist('gallery'):
#                 Image.objects.create(album=event.album, image=file, title=file.name)
#
#             return redirect(event.get_absolute_url()) # get_absolute_url pouziva slug
#
#     return render(request, 'events/update_event.html',
#          {'event_form': event_form, 'file_form': file_form, 'event': event})


class UpdateEventView(UserPassesTestMixin, MessageActionMixin, UpdateView): # ProcessFormView.post(): if form.is_valid()==True-->UpdateEventView.form_valid() ulozi data z formu do db -->MessageActionMixin.form_valid() ulozi do requestu messages --> UpdateEventView.form_valid() udela redirect
                                                                            #                         if form.is_valid()==False-->MessageActionMixin.form_invalid() ulozi do requestu messages -->FormMixin.form_invalid() vraci stejnou stranku s form.errors a request._messages
                                                                            # UserPassesTestMixin nahrazuje GroupRequiredMixin -> autorizovany user bude jen ten, ktery vytvoril event, nikoliv vsichni hostitele
    model = Event
    form_class = EventForm
    template_name = 'events/update_event.html'
    success_message = 'The event %(name)s has been updated successfully'
    error_message = 'The event %(name)s could not be updated'

    permission_denied_message = "Too bad, you don't have access to these lands" # overriduje AccessMixin.permission_denied_message (UserPassesTestMixin dedi od AccessMixin)

    def test_func(self): # overriduje AccessMixin.test_func() # vraci False pokud requestujici user neodpovida hostiteli daneho eventu
        event = self.get_object()
        return self.request.user.id == event.host.id

    def handle_no_permission(self): # overriduje AccessMixin.handle_no_permission(), ktera by v pripade AnonymousUsera redirectovala na login
        raise PermissionDenied(self.get_permission_denied_message()) # zobrazi "403 Forbidden" (spousti view: django.views.defaults.permission_denied), pokud chceme zobrazit permission_denied_message (custom),
                                                                    # je nutne umistit soubor 403.html do templates/ a do nej vlozit kod napr.:  {% if exception %}  <p>{{ exception }}</p> {% else %} <p>Static generic message</p> {% endif %}!!

    def form_valid(self, form):

        event = form.save()

        for file in self.request.FILES.getlist('gallery'):
            Image.objects.create(album=event.album, image=file, title=file.name)

        super().form_valid(form)

        return redirect(event.get_absolute_url())


# @login_required
# def delete_event(request, slug):
#
#     Event.objects.get(slug=slug)
#
#     return redirect('events:my_events_listing')


class DeleteEventView(UserPassesTestMixin, MessageActionMixin, DeleteView):
    model = Event
    success_url = reverse_lazy('events:my_events_listing') # success_url je stranka, kam se redirectuje po deletovani objektu
                                                    # DeleteView.get() musi dostat event_confirm_delete.html - potvrzovaci stranka, zda skutecne chceme deletovat objekt
                                                    # reverse_lazy - URL vytvoreno, az kdyz metoda zavolana, ne hned
    success_message = "The event %(name)s has been removed successfully"

    permission_denied_message = "Too bad, you don't have access to these lands"

    def test_func(self):
        event = self.get_object()
        return self.request.user.id == event.host.id

    def handle_no_permission(self):
        raise PermissionDenied(self.get_permission_denied_message())


# @login_required
# def update_run(request, slug, run_id):
#
#     updated_run = EventRun.objects.get(event__slug=slug, id=run_id) #to, co se predvyplni ve form
#
#     if request.method == 'POST':
#
#         form = EventRunForm(data=request.POST)
#
#         if form.is_valid():
#
#             run = form.save(commit=False)
#             run.id = int(run_id)
#             run.event=Event.objects.get(slug=slug)
#             run.save()
#             #updated_event.delete()
#
#             return redirect('events:event_detail', slug)
#
#         return render(request, 'events/update_run.html', {'form': form} )
#
#     form = EventRunForm(instance=updated_run)#predvyplneni
#     return render(request, 'events/update_run.html', {'form': form} )


class UpdateEventRunView(UserPassesTestMixin, MessageActionMixin, UpdateView): # ProcessFormView.post(): if form.is_valid()==True-->UpdateEventView.form_valid() ulozi data z formu do db -->MessageActionMixin.form_valid() ulozi do requestu messages --> UpdateEventView.form_valid() udela redirect
                                                                            #                         if form.is_valid()==False-->MessageActionMixin.form_invalid() ulozi do requestu messages -->FormMixin.form_invalid() vraci stejnou stranku s form.errors a request._messages
    model = EventRun
    form_class = EventRunForm
    template_name = 'events/update_run.html'
    pk_url_kwarg = "run_id" # argument zaslany do views z urls, pokud definovan, get_object() ho pouzije k vyhledani objektu
    success_message = 'The event run for event %(name)s has been updated successfully'
    error_message = 'The event run for event %(name)s could not be updated'

    permission_denied_message = "Too bad, you don't have access to these lands" # overriduje AccessMixin.permission_denied_message (UserPassesTestMixin dedi od AccessMixin)

    def test_func(self):
        eventrun = self.get_object()
        return self.request.user.id == eventrun.event.host.id

    def handle_no_permission(self):
        raise PermissionDenied(self.get_permission_denied_message())

    def form_valid(self, form):

        self.object = form.save()

        super().form_valid(form)

        return redirect(self.object.event.get_absolute_url())

    def get_success_url(self):

        return self.object.event.get_absolute_url()


# @login_required
# def delete_run(request, slug, run_id):
#
#     deleted_run = EventRun.objects.get(event__slug=slug, id=run_id)
#     deleted_run.delete()
#
#     return redirect('events:event_detail', slug)


class DeleteEventRunView(UserPassesTestMixin, MessageActionMixin, DeleteView):
    model = EventRun
    pk_url_kwarg = "run_id"
    success_message = "The event run for event %(name)s has been removed successfully"

    permission_denied_message = "Too bad, you don't have access to these lands" # overriduje AccessMixin.permission_denied_message (UserPassesTestMixin dedi od AccessMixin)

    def test_func(self):
        eventrun = self.get_object()
        return self.request.user.id == eventrun.event.host.id

    def handle_no_permission(self):
        raise PermissionDenied(self.get_permission_denied_message())

    def get_success_url(self):

        return self.object.event.get_absolute_url()


# def run_detail(request, event_id, run_id):
#
#     runs = EventRun.objects.all()
#
#     for run in runs:
#         if str(run.id)==run_id:
#
#             return render(request, 'events/run_detail.html', {'run': run})
#
# def runs_listing(request, event_id):
#
#     runs = EventRun.objects.filter(event_id=event_id)
#
#     return render(request, 'events/runs_listing.html', {'runs': runs})


# Create your views here.
