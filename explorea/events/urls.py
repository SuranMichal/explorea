from django.urls import path

from . import views

app_name = 'events'

urlpatterns = [
    #path('new/', views.add_event, name='add_event'),
    path('new/', views.AddEventView.as_view(), name='add_event'),
    #path('mine/', views.my_events_listing, name='my_events_listing'),
    path('mine/', views.MyEventsView.as_view(), name='my_events_listing'),
    #path('search/', views.event_search, name='search'),
    path('search/', views.EventSearchView.as_view(), name='search'),

    #path('detail/<slug:slug>/', views.event_detail, name='event_detail'),#slug je samostatny type (jako str nebo int)
    path('detail/<slug:slug>/', views.EventDetailView.as_view(), name='event_detail'),
    #path('update/<slug:slug>/', views.update_event, name='update_event'),#chybejici lomitko na konci nesmyslne spousti nasledujici pattern!
    path('update/<slug:slug>/', views.UpdateEventView.as_view(), name='update_event'),
    #path('delete/<slug:slug>/', views.delete_event, name='delete_event'),
    path('delete/<slug:slug>/', views.DeleteEventView.as_view(), name='delete_event'),

    #path('<slug:slug>/update_run/<str:run_id>/', views.update_run, name='update_run'),
    path('<slug:slug>/update_run/<str:run_id>/', views.UpdateEventRunView.as_view(), name='update_run'),
    #path('<slug:slug>/delete_run/<str:run_id>/', views.delete_run, name='delete_run'),
    path('<slug:slug>/delete_run/<str:run_id>/', views.DeleteEventRunView.as_view(), name='delete_run'),
    #path('<slug:slug>/add_run/', views.add_run, name='add_run'),
    path('<slug:slug>/add_run/', views.AddEventRunView.as_view(), name='add_run'),

    #path('<str:event_id>/runs/', views.runs_listing, name='runs_listing'),
    #path('<str:event_id>/<str:run_id>/', views.run_detail, name='run_detail'),

    #path('<category>/', views.events_listing, name='events_listing'),
    path('<category>/', views.EventListView.as_view(), name='events_listing'),

]
