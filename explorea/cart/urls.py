from django.urls import path

from . import views

app_name = 'cart'

urlpatterns = [
    path('', views.CartDetailView.as_view(), name='detail'),
    path('add/<slug:slug>/', views.CartAddView.as_view(), name='add'),
    #path('remove/<product_id>/', views.CartRemoveView.as_view(), name='remove'),
    path('remove/<int:product_id>/', views.cart_remove, name='remove'),

]
