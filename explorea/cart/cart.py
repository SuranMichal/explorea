from django.conf import settings
from django.contrib import messages

from explorea.events.models import EventRun

from . forms import CartAddForm

# takto budou data cartu organizovana v session: session = { ..., 'cart': {23: 2, 2: 1}, ...} # napr. product_id = 23, quantity = 2
# --> zmena! 'cart': {23: {"quantity": 2,"..": ..}, 2: {..}}

class Cart:
    def __init__(self, session):
        self.session = session
        self.cart = session.get(settings.CART_SESSION_ID) # CART_SESSION_ID = "cart" --> vytahne ze session dict klic cart
        if not self.cart:
            self.cart = self.session[settings.CART_SESSION_ID] = {} # pokud nenajde v session klic "cart", vytvori ho a priradi value = {}

    def __len__(self):
        return len(self.cart)

    def is_empty(self):
        return len(self.cart) == 0

    def add(self, product_id, quantity=1, update=False, **kwargs): # pridavani polozek do kosiku
        product_id = str(product_id)
        if product_id in self.cart and not update:
            self.cart[product_id]["quantity"] += quantity # self.cart.values() jsou slovniky, value klice quantity se zvysi o argument quantity

        else:
            self.cart.update({str(product_id): {"quantity":quantity}}) # vytvori se novy klic v self.cart a priradi se mu slovnik

        self.save()

    def remove(self, product_id, quantity=1):
        product_id = str(product_id)
        if product_id in self.cart:
            if quantity < self.cart[str(product_id)]["quantity"]: # moje verze, z kosiku lze odebirat po quantity
                self.cart.update({str(product_id): {"quantity":self.cart[str(product_id)]["quantity"]-quantity}})
            else:
                self.cart.pop (product_id, 0)
        self.save()

    def __getitem__(self, item): # nebyt teto metody, test_cart_add_item() vyhodi: "Cart object is not subscriptable" --> implementuje moznost indexovani u objektu teto tridy
        return self.cart[str(item)] # nyni lze ziskat quantity jako napr. c["15"] nebo c[15], kde c je instance cartu a "15" je item, neboli product_id

    def __contains__(self, item): # implementuje moznost zjistovat zda je polozka v kosiku
        return item in self.cart    # napr. "15" in c

    def __eq__(self, other): # vraci True pokud other je kosik s identickym obsahem
        return self.cart == other # napr. b={"20":1,"15":1}, c==b --> True

    def __iter__(self): # tuto metodu spousti napr. for cyklus ve views -->
        event_runs = EventRun.objects.filter(id__in=self.cart.keys()) # vraci qs tech event runu, jejichz id obsahuje kosik

        for event_run in event_runs:
            self.cart[str(event_run.id)]["object"] = event_run # do vsech slovniku self.cart.values() prida klic object, jehoz value jsou event runy odpovidajici product_id v self.cart
                                                        # pridani objektu EventRun do self.cart nam umozni pristupovat k atributum EventRun modelu (i navazanych modelu, tj. Event, User) !!
        for item_id, data in self.cart.items():
            data["total_price"] = data["quantity"] * data["object"].price # v self.cart.values() vytvori novy klic total_price
            yield data  # napr. c = iter(cart), next(c)  --> {'quantity': 1, 'object': <EventRun: 2019-11-18|LeoExpress booking (SI)>, 'total_price': Decimal('13.00')}
                                            # cart.cart --> {'15': {'quantity': 1, 'object': <EventRun: 2019-11-18|LeoExpress booking (SI)>, 'total_price': Decimal('13.00')}, '16': {'quantity': 1, 'object': <EventRun: 2019-03-03|Prochazka (RX)>}}
                                            #   next(c)  --> {'quantity': 1, 'object': <EventRun: 2019-03-03|Prochazka (RX)>, 'total_price': Decimal('0.70')}
                                            # cart.cart --> {'15': {'quantity': 1, 'object': <EventRun: 2019-11-18|LeoExpress booking (SI)>, 'total_price': Decimal('13.00')}, '16': {'quantity': 1, 'object': <EventRun: 2019-03-03|Prochazka (RX)>, 'total_price': Decimal('0.70')}}

    def save(self): # spolecny kod u metod add a remove: self.session.modified = True vlozime do teto metody
        self.session[settings.CART_SESSION_ID] = self.cart # modifikovany obsah kosiku se ulozi do session
        self.session.modified = True # musi byt, jinak se do session ulozi jen prvni vlozena polozka a dalsi uz ne !!! v tomto pripade je totiz modifikovan pouze request.session["cart"], nikoliv request.session !

    def get(self, product_id): # pokud product_id neni nalezen v kosiku, nevyhodi error, ale vrati 0
        try:
            return self.cart[str(product_id)]["quantity"]
        except KeyError:
            return 0

    def prepare_to_render(self, update):

        for item in self: # for cyklus spusti self.__iter__(), ktera obohacuje self
            form_initial = {
                'quantity':item['quantity'],
                'update': update
            }
            item['cartadd_form'] = CartAddForm(initial=form_initial)
        # v tuto chvili self = napr. {'17': {'quantity': 2, 'object': <EventRun: 2019-05-08|CzechOutdoor (FN)>, 'total_price': Decimal('4.00'), 'cartadd_form': <CartAddForm bound=False, valid=Unknown, fields=(quantity;product_id;update;current_quantity)>}, '22': {'quantity': 1, 'object': <EventRun: 2019-03-18|CzechOutdoor (FN)>, 'total_price': Decimal('19.00'), 'cartadd_form': <..>}}
