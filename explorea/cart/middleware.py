from . cart import Cart

class CartMiddleware:

    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs): # tato metoda se pousti pri zavolani KAZDEHO view, tj. request.cart se vytvori zavolanim JAKEHOKOLIV view --> tj. po zavolani jakehokoliv view bude request.cart.cart = {}
        request.cart = Cart(request.session) # provede se pred zavolanim view, tj. do vsech views bude vstupovat request s atributem cart
                                             # instanci Cart pripojime na request, protoze je to objekt ktery vstupuje jak do metod middlewaru, tak i views !!

    def process_template_response(self, request, response): # tato metoda se pousti pri zavolani KAZDEHO view --> promenna cart (viz nize) je k dispozici v kontextu KAZDE response !
        cart_update_views = ["CartDetailView",]

        update = response.context_data.get("view", None).__class__.__name__ or None in cart_update_views # response.context_data["view"] je view, ktere vratilo response, tj. napr. <explorea.events.views.EventDetailView object at ..>
                                                                                       # --> pouze v pripade CartDetailView je update=True, jinak update=False
        request.cart.prepare_to_render(update=update) # provede se obohaceni request.cart
        response.context_data ["cart"] = request.cart # ulozeni request.cart do kontextu response
        return response
