from django import forms

from explorea.events.models import EventRun

class PositiveIntegerField(forms.IntegerField):

    def validate(self, value):
        super().validate(value)
        if value < 1:
            raise forms.ValidationError('Only positive integer allowed')

class CartAddForm(forms.ModelForm): # kdyz chceme dostat data z modelu do formu, pouzijeme ModelForm misto Form
    quantity = PositiveIntegerField(initial=0) # initial specifikuje vychozi hodnotu pro unbound form; posleme-li prazdny form, bude stale unbound
    product_id = forms.CharField(max_length=10, widget=forms.HiddenInput()) # nebude zobrazeno na strance (neni treba)
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput())  # input pro volbu rezimu update ve funkci Cart.add()
    current_quantity = forms.IntegerField(required=False, initial=0, widget=forms.HiddenInput()) # pocet polozek u product_id, ktere uz v kosiku jsou
                                                                                                # initial=0 nevstupuje do cleaned_data, protoze tento atribut je urcen pro unbound form - atribut cleaned_data vznika pri validaci po odeslani formu (POST --> form je bound) a tim padem obsahuje pro current_quantity prazdnou hodnotu, coz je pro IntegerField None
    # quantity zadavam do formu, product_id je definovane zvolenym polickem
    class Meta:
        model = EventRun
        fields = [] # zadne pole z modelu se neobjevi ve formu

    def clean(self):
        super().clean() # provede se django default clean() na formu a vytvori se cleaned_data pro jeho fieldy
        quantity = self.cleaned_data.get("quantity") # v pripade, ze zadame do formu quantity=0: form je invalid, quantity se nenachazi v cleaned_data --> tzn. self.cleaned_data.get("quantity")=None, ale nevyhodi KeyError jako self.cleaned_data["quantity"]
        product_id = self.cleaned_data.get("product_id")
        update = self.cleaned_data.get("update") or self.fields['update'].initial
        current_quantity = 0 if update else self.cleaned_data.get("current_quantity") or self.fields['current_quantity'].initial  # pokud update=True, current_quantity se nastavi na 0

        if all ([quantity, product_id]) and current_quantity >= 0: # all(iterable) - pokud jak quantity, tak product_id jsou True (coz znamena predevsim, ze nejsou None, tzn. False, coz je pripad kdy nejsou v cleaned_data), vraci True
            try:
                run = self._meta.model.objects.get(pk=int(product_id))
                if run.seats_available < int(quantity) + int(current_quantity):
                    self.add_error ("quantity", "Quantity cannot exceed number of available seats") # errory jsou pri form.clean() by default prirazeny do non_field_errors (field = __all__), chceme-li priradit form error konkretnimu fieldu musime pouzit add_error
                                                                                                    # Form.add_error(field, error), automaticky maze odpovidajici field z cleaned_data
            except self._meta.model.DoesNotExist:
                self.add_error ("product_id", "Requested item does not exist")
