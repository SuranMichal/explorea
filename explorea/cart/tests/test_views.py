from datetime import date, timedelta, time

from django.test import TestCase
from django.urls import resolve, reverse
from unittest import skip
from django.contrib.auth import get_user_model

from explorea.events.models import Event, EventRun
from ..views import CartDetailView, CartAddView # CartDetailView - view, ktere budeme testovat; test-driven approach - toto view jeste neexistuje, nejdriv piseme testy
from ..forms import CartAddForm

UserModel = get_user_model()

class CartDetailViewTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # fix login
        super().setUpClass() # vzdy pokud definujeme custom setUpClass, musime volat super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username,
                                                 password=cls.password)
        # allow showing cart detail
        cls.event1 = Event.objects.create(host=cls.user,
                                         name='Test Event1',
                                         description='Test Event1 Description',
                                         location='Test Event1 Location')

        cls.eventrun1 = EventRun.objects.create(event=cls.event1,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=20,
                                               price=200)

        cls.event2 = Event.objects.create(host=cls.user,
                                        name='Test Event2',
                                        description='Test Event2 Description',
                                        location='Test Event2 Location')

        cls.eventrun2 = EventRun.objects.create(event=cls.event2,
                                                date=date.today() + timedelta(days=3),
                                                time=time(hour=11, minute=30),
                                                seats_available=20,
                                                price=200)

    def setUp(self):
        self.client.login(username=self.username, password=self.password) # v pripade pouzivani Django authentication system a standard authentication backend - vzdy vyzaduje kwargs username, password; user musi byt predem vytvoreny funkci create_user

    def test_cart_detail_url_resolves_to_cart_detail_view(self):
        page = resolve(reverse('cart:detail')) # resolve vyhodnoti, co se skryva pod danym URL - ResolverMatch(func=explorea.events.views.AddEventView, args=(), kwargs={}, url_name=add_event, app_names=['events'], namespaces=['events'])
        self.assertEqual(page.func.__name__, CartDetailView.__name__) # page.func.__name__ vraci nazev view
                                                                    # assertEqual (a,b) / lze napsat jako assert a == b / vyhodnoti zda a == b --> vraci bud nic (True), nebo vyhodi AssertionError (False)
                                                                    # aby bylo mozne spustit testy prikazem: python manage.py test explorea.cart.tests, je potreba, aby v explorea/explorea byl prazdny __init__.py, protoze Django pouzije neco jako: FROM explorea.cart.tests IMPORT *, je tedy potreba, aby explorea byl package
                                                                    # prikaz python manage.py test <adresar> vyhleda a spusti vsechny soubory s nazvem test*.py v <adresar>

    def test_cart_detail_shows_no_items_when_empty(self):
        response = self.client.get(reverse('cart:detail')) # client je Client(), simuluje posilani requestu, tzn. interaguje s Django serverem (GET,POST,..)
                                                            # Client byl vytvoren Django komunitou pro TestCase (TestCase patri unittestu)
        self.assertIn(b'The cart is empty', response.content) # response je v html, tzn. vyzaduje html template, kam by mohla renderovat dany bytes string

    def test_cart_detail_shows_items_when_not_emtpy(self):

        data1 = {'quantity': 2, 'product_id': self.eventrun1.id, "current_quantity": 0}
        self.client.post (reverse('cart:add', args=[self.event1.slug]), data1) # data se poslou primo jako request.POST na napr. ../cart/add/prochazka-with-ab/

        data2 = {'quantity': 3, 'product_id': self.eventrun2.id, "current_quantity": 0}
        self.client.post (reverse('cart:add', args=[self.event2.slug]), data2)

        response = self.client.get(reverse('cart:detail'))
        self.assertIn(self.event1.name.encode(), response.content) # encode() koduje do bytes
        self.assertIn(self.event2.name.encode(), response.content)

    @skip # test runner tuto metodu nespusti
    def test_cart_detail_shows_remove_buttons_if_not_empty(self):
        pass

class CartAddViewTest(TestCase):

    @classmethod
    def setUpClass(cls): # vytvarime objekty, na kterych budeme testovat, jako atributy testovaci tridy
                        # setUpClass (tearDown) je na rozdil od setUp(tearDownClass) spustena jen jednou pro kazdou tridu (pro kazdy test case), ne pro kazdou metodu
                        # objekty, ktere vytvorime v db, jsou po dokonceni testcase zase z db smazany
        super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username, password=cls.password)

        cls.event = Event.objects.create(host=cls.user,
                                         name='Test Event',
                                         description='Test Event Description',
                                         location='Test Event Location')
        cls.eventrun = EventRun.objects.create(event=cls.event,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=20,
                                               price=200)

    def setUp(self):
        self.view = CartAddView
        self.url = reverse('cart:add', args=[self.event.slug]) # self.event.slug --> slug vytvoreny pro Test Event (vytvori se pri zavolani save() behem vytvareni eventu)
        self.client.login(username=self.username, password=self.password)

    def test_cart_add_url_resolves_to_cart_add_view(self):
        page = resolve(self.url)
        self.assertEqual(page.func.__name__, self.view.__name__)

    def test_cart_add_view_valid_form_data_redirects_to_corresponding_product_page_with_success(self):
        response = self.client.post(self.url,
                                   data={'quantity': self.eventrun.seats_available, 'product_id': self.eventrun.id, 'current_quantity': 0},
                                   follow=True) # follow-True: pokud by self.url nekam redirectovala, data se preposlou na vsechny URL v response.redirect_chain
        self.assertRedirects(response, self.event.get_absolute_url()) # response.redirect_chain = [('/events/detail/test-event-with-test/', 302)]

        msg = list(response.context.get('messages'))[0]
        self.assertEqual(msg.message, 'The run has been added to the cart')
        self.assertEqual(msg.tags, 'success')

    def test_cart_add_view_invalid_form_data_redirects_to_corresponding_product_page_with_error(self):
        response = self.client.post(self.url,
                                   data={'quantity': self.eventrun.seats_available+1, 'product_id': self.eventrun.id, 'current_quantity': 0},
                                   follow=True)

        self.assertRedirects(response, self.event.get_absolute_url())

        msg = list(response.context.get('messages'))[0] # msg = <django.contrib.messages.storage.base.Message object at ..>, msg.__dict__ = {'level': 25, 'message': 'The run has been added to the cart', 'extra_tags': None}
        self.assertEqual(msg.message, 'The run could not be added into the cart')
        self.assertEqual(msg.tags, 'error')

class CartRemoveViewTest(TestCase):

    @classmethod
    def setUpClass(cls): # vytvarime objekty, na kterych budeme testovat, jako atributy testovaci tridy
                        # setUpClass (tearDown) je na rozdil od setUp(tearDownClass) spustena jen jednou pro kazdou tridu (pro kazdy test case), ne pro kazdou metodu
                        # objekty, ktere vytvorime v db, jsou po dokonceni testcase zase z db smazany
        super().setUpClass()
        cls.user = UserModel.objects.create_user(username='TEST', password='password123')
        cls.event = Event.objects.create(host=cls.user,
                                         name='Test Event',
                                         description='Test Event Description',
                                         location='Test Event Location')
        cls.eventrun = EventRun.objects.create(event=cls.event,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=20,
                                               price=200)

    @skip # test runner tuto metodu nespusti
    def test_cart_remove_removes_one_instance_of_one_item_by_default(self):
        pass
