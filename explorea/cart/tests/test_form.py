from datetime import date, timedelta, time

from django.test import TestCase
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

from ..forms import CartAddForm, PositiveIntegerField

from explorea.events.models import Event, EventRun

UserModel = get_user_model()

class PositiveIntegerFieldTest(TestCase):

    def test_validate_positive_integer(self):
        field = PositiveIntegerField()
        self.assertIsNone(field.validate(2))

    def test_validated_integer_less_than_one(self):
        field = PositiveIntegerField()

        with self.assertRaises(ValidationError):
            field.validate(-5) # pokud field.validate(-5) vyhodi ValidationError, test PASSES; jiny zapis: self.assertRaises(ValidationError, field.validate, -5)
                                # with self.assertRaises(ValidationError) as contextman: field.validate(-5); exception=contextman.exception --> ValidationError(['Only positive integer allowed'])

class CartAddFormTest(TestCase):

    @classmethod
    def setUpClass(cls): # vytvarime objekty, na kterych budeme testovat, jako atributy testovaci tridy
                        # setUpClass (tearDown) je na rozdil od setUp(tearDownClass) spustena jen jednou pro kazdou tridu (pro kazdy test case), ne pro kazdou metodu
                        # objekty, ktere vytvorime v db, jsou po dokonceni testcase zase z db smazany
        super().setUpClass()
        cls.user = UserModel.objects.create_user(username='TEST', password='password123')
        cls.event = Event.objects.create(host=cls.user,
                                         name='Test Event',
                                         description='Test Event Description',
                                         location='Test Event Location')
        cls.eventrun = EventRun.objects.create(event=cls.event,
                                               date=date.today() + timedelta(days=2),
                                               time=time(hour=10, minute=30),
                                               seats_available=3,
                                               price=200)

    def test_form_valid_quantity_positive_int_id_exists(self):
        data = {'quantity': 1, 'product_id': 1, "current_quantity": 0} # pridani jedne polozky do kosiku by melo projit (from valid)

        form = CartAddForm(data=data)

        self.assertTrue(form.is_valid())

    def test_form_invalid_quantity_less_than_one(self):
        data = {'quantity': 0, 'product_id': 1, "current_quantity": 0} # pridani 0 polozek do kosiku by melo vyhodit chybu (from invalid)

        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid()) # pridani 0 polozek do kosiku by melo vyhodit chybu (from invalid)
        self.assertIsNotNone(form.errors.get('quantity')) # v cart/forms.py je definovano, ze ma byt vyhozen ValidationError s hlaskou 'Only positive integer allowed',
                                                        # testuje tedy, zda na fieldu quantity ve form.errors je nejaky error
        self.assertIn('Only positive integer allowed', form.errors.get('quantity')) # testuje, zda na fieldu quantity ve form.errors je error s danou hlaskou

    def test_form_rendered_invalid_non_existent_id(self):
        data = {'quantity': 1, 'product_id': -1, "current_quantity": 0}

        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('product_id'))
        self.assertIn('Requested item does not exist', form.errors.get('product_id'))

    def test_form_invalid_updating_to_quantity_greater_than_avaialable(self):
        data = {'quantity': 20, 'product_id': 1, "current_quantity": 0, "update":True}

        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('quantity'))
        self.assertIn('Quantity cannot exceed number of available seats', form.errors.get('quantity'))

    def test_form_invalid_updating_to_quantity_greater_than_avaialable_current_quantity_more_than_zero(self):
        data = {'quantity': 5, 'product_id': 1, "current_quantity": 2, "update":True}

        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('quantity'))
        self.assertIn('Quantity cannot exceed number of available seats', form.errors.get('quantity'))

    def test_form_invalid_adding_quantity_more_than_available(self):

        data = {'quantity': 20, 'product_id': 1, "current_quantity": 0}

        form = CartAddForm(data=data)

        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get('quantity'))
        self.assertIn('Quantity cannot exceed number of available seats', form.errors.get('quantity'))
