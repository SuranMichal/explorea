from django.test import TestCase
from django.contrib.sessions.backends.db import SessionStore
from django.conf import settings

from unittest import skip

from ..cart import Cart

class CartTest(TestCase):

#    @classmethod
#    def setUpClass(cls): # pokud vytvorime setUpClass - a definujeme cls.product_id, zmenime product_id na self.product_id
#        super().setUpClass()
#        cls.product_id = "2" # to bude id vybraneho eventu
#        cls.quantity = 1

    def setUp(self): # je definovana v unitest class TestCase; patri k tzv.fixtures - sem se dava kod, ktery by bylo nutne opakovat v kazde custom TestCase metode
                    # dame-li spolecny kod do setUp, musime zajistit, aby byl pristupny pro vsechny funkce --> pokud berou self, musime session a cart na nej navazat --> self.session, self.cart
        self.session = SessionStore() # SessionStore je trida Djanga, ktera se stara o zapisovani a vytahovani dat z/do db; v realu nam prijde objekt SessionStore zvenci od request.session (nebudeme ho vytvaret)
        self.cart = Cart(self.session) # pred kazdym zavolanim testovaci metody se spusti setUp - ten vytvori pokazde novy objekt SessionStore a novy objekt Cart

    def test_create_cart(self):

        self.assertTrue(hasattr(self.cart, 'session')) # testujeme, zda Class Cart ma atribut session
        self.assertIsInstance(self.cart.session, SessionStore) # request.session ma vsak byt instance SessionStore
        self.assertTrue(hasattr(self.cart, 'cart')) # atribut cart ma odkazovat v session na obsah kosiku, ma byt type dict
        self.assertIsInstance(self.cart.cart, dict) # self.cart je instance Cart! (self je instance TestCase) --> proto self.cart.cart je atribut cart instance Cart

    def test_new_cart_is_empty(self):

        self.assertTrue(self.cart.is_empty())

    def test_cart_add_item(self):
        product_id = 15 # to bude id vybraneho eventu
        quantity = 5

        self.cart.add(product_id=product_id, quantity=quantity)
        self.assertEqual(self.cart[product_id]["quantity"], quantity)

    def test_cart_add_adds_one_item_by_default(self):
        product_id = 15

        self.cart.add(product_id=product_id)
        self.assertEqual(self.cart[product_id]["quantity"], 1)

    def test_cart_add_updates_session(self):
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=quantity)

        self.assertEqual(self.session[settings.CART_SESSION_ID][str(product_id)],
                         self.cart[product_id])

    def test_cart_add_updates_to_correct_quantity_if_update_true(self): # vlastni verze, engeto: vysledna quantity se ma rovnat 1, neporovnava session
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=quantity)
        self.cart.add(product_id=product_id, quantity=quantity, update=True)
        self.assertEqual(self.session[settings.CART_SESSION_ID][str(product_id)]["quantity"], quantity)

    def test_cart_add_changes_quantity_if_update_false(self):
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=quantity)
        self.cart.add(product_id=product_id, quantity=quantity, update=False)
        self.assertEqual(self.cart[product_id]["quantity"], 2)

    def test_cart_remove_removes_one_item_by_default(self):
        product_id = 15

        self.cart.add(product_id=product_id, quantity=2)
        self.cart.remove(product_id=product_id)

        self.assertEqual(self.cart[product_id]["quantity"], 1)

    def test_cart_remove_item_not_in_cart_if_remove_quantity_more_than_add_quantity(self):
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=quantity)
        self.cart.remove(product_id=product_id, quantity=2)

        self.assertNotIn(str(product_id), self.session[settings.CART_SESSION_ID])

    def test_cart_remove_item_quantity_decreases_correctly_if_remove_quantity_less_than_add_quantity(self):
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=2)
        self.cart.remove(product_id=product_id, quantity=quantity)
        self.assertEqual(self.session[settings.CART_SESSION_ID][str(product_id)]["quantity"], 1)

    def test_cart_remove_handles_non_existent_id(self):
        product_id = 15
        quantity = 1

        self.cart.add(product_id=product_id, quantity=quantity)
        self.cart.remove(product_id=99, quantity=35)

        self.assertEqual(self.cart, {"15":{"quantity":1}})
