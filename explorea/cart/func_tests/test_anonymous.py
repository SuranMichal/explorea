import os
from django.conf import settings

from django.test import LiveServerTestCase
from django.urls import reverse

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException

class AnonymousUserTest(LiveServerTestCase): # LiveServerTestCase spusti live Django server na localhost pri setUp (port 0 - OS priradi pokazde jiny z free portu?), zavre pri tearDown; self.live_server_url - vrati URL serveru
    fixture_files = ['users.json', 'events.json', 'eventruns.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.fixtures =[os.path.join(settings.FIXTURE_DIRS[0],f) # vytvori se atribut AnonymousUserTest.fixtures = [".../explorea/fixtures/users.json","...",".../explorea/fixtures/eventruns.json"]
                        for f in cls.fixture_files]              # vytvorime-li na class LiveServerTestCase attr fixtures, Django bude za nas importovat data v souborech JSON

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.wait = WebDriverWait(self.browser, 1) # WebDriverWait instance - pouzije browser a pokud se bude volat jeho metoda (napr. until()), zkousi to do/ceka 1 sekundu; pokud je vysledek neuspesny, vyhodi TimeoutException

    def tearDown(self): # provede se PO spuštění každé testovací metody, zavře browser
        self.browser.close()

    def test_anonymous_user(self):
        # New visitor enters the url of detail_view into
        # the browser
        self.browser.get(self.live_server_url + reverse('cart:detail')) # self.browser provadi GET, POST requesty # timto je nactena URL s template cart_detail.html

        # She sees the detail page announcing there is nothing
        # in the shopping cart
        self.assertIn('Cart Detail', self.browser.title) # overujeme zda se v cart_detail.html v <title> nachazi text 'Cart Detail'

        # The visitor decides to go to the events page where
        # she selects the first event available
        events_link = self.browser.find_element_by_css_selector('#nav-buttons a:first-of-type')
        events_link.click()

        # Visitor is taken to the events page
        self.assertIn('Event Listing', self.browser.title)

        # Visitor select's the first event
        event = self.wait.until(lambda driver: driver.find_element(By.CLASS_NAME, 'event-item:first-of-type')) # nazev parametru driver neni podstatny, pri exekuci lambda funkce je nahrazen jakymkoliv vstupujicim argumentem
                                                                                        # event --> <selenium.webdriver.remote.webelement.WebElement (session="...", element="0.47375679559653716-1")>

        event.send_keys(Keys.RETURN) # simulace stlaceni ENTER na klavesnici; class Keys obsahuje konstanty odpovidajici ruznym klavesam; uziti tez napr. send_keys("abc") zada do formulare "abc"

        # The visitor does not have access to any delete nor update event nor event run links
        self.assertRaises(
            TimeoutException,
            lambda: self.wait.until(lambda driver: driver.find_element(By.CLASS_NAME, 'edit-button')) # self.assertRaises (<Exception>,<nazev_callable>)
        )                  # until () - neustale spousti funkci (v tomto pripade po dobu 1s), bere metodu driveru jako input; vraci bud value, kterou vraci metoda driveru (v tomto pripade find_element()), nebo raisuje TimeoutException
                        # funkce vyhledava na strance element podle class = 'edit-button' # find_element_by_css_selector ('.edit-button') ODPOVIDA find_element(By.CLASS_NAME, 'edit-button')
                            # pokud anonymous user skutecne nevidi edit linky, tak se nepodari element najit --> until() vyhodi TimeoutException, coz assertRaises bere jako TEST PASSED a exekuce kodu pokracuje na dalsim radku
        endpoint = reverse('events:event_detail', args=[None]).rsplit('/', 2)[0] # rsplit () oddeli ve vygenerovanem URL zprava 2 slova oddelovacem / a ulozi vysledek do listu
                                                                                # args=[None] je tam proto, ze reverse bez args neni definovana v urlconf a hazi NoReverseMatch
        self.assertIn(endpoint, self.browser.current_url) # proveruje zda je URL "events:event_detail" obsazene v URL aktualne nactene stranky

        # Looking at the detail element page, the visitor sees the possibility of adding event run into a cart
        add_cart_button = self.browser.find_element(By.CSS_SELECTOR, '.cart-add:first-child button')

        # The visitor accidentally clicks on the Add to Cart button keeping
        # the number of items on 0
        add_cart_button.click()

        # The visitor is shown the event detail page with an error message
        # that the run could not be added into the cart
        flash = self.wait.until(lambda driver: driver.find_element(By.CLASS_NAME, 'error')) # flash ma obsahovat message, je to WebElement, flash.tag_name = "li", flash.text je 'The run could not..'
        self.assertIn('The run could not be added into the cart', flash.text)               # messages jsou v base.html, po headeru, jako <ul> class = "messages", jejich typ pod <li> id=.. kazde message jako class="{{ message.tags }}"

        endpoint = reverse('events:event_detail', args=[None]).rsplit('/', 2)[0]
        self.assertIn(endpoint, self.browser.current_url)

        # The visitor now sets the number of runs to 1 and clicks on Add to Cart
        # button
        quantity_input = self.browser.find_element(By.CSS_SELECTOR, '.cart-add:first-child #id_quantity')
        quantity_input.clear()
        quantity_input.send_keys("1")
        add_cart_button = self.browser.find_element(By.CSS_SELECTOR,
                                     '.cart-add:first-child button')
        add_cart_button.click()

        # The visitor gets the flash message that the event has been added to the cart
        # but stays on the same event detail page
        flash = self.wait.until(lambda driver: driver.find_element(By.CLASS_NAME, 'success'))
        self.assertIn('The run has been added to the cart', flash.text)

        endpoint = reverse('events:event_detail', args=[None]).rsplit('/', 2)[0]
        self.assertIn(endpoint, self.browser.current_url)

#       self.fail('TEST END')
        # The visitor decides to put another event into the cart

        quantity_input = self.browser.find_element(By.CSS_SELECTOR, 'ul:nth-of-type(2) li:nth-of-type(2) .cart-add #id_quantity')
        quantity_input.clear()
        quantity_input.send_keys("1")
        add_cart_button = self.browser.find_element(By.CSS_SELECTOR, 'ul:nth-of-type(2) li:nth-of-type(2) .cart-add button')
        add_cart_button.click()

        # The visitor now decides to check that cart

        cart_detail_link = self.wait.until(lambda driver: driver.find_element(By.CSS_SELECTOR, ".header-section#user-buttons .nav-button:first-of-type")) # zde id selector musi byt nalepeny bez mezery!?
        cart_detail_link.send_keys(Keys.RETURN)

        # The visitor is taken to cart setail page and see the content of cart

        endpoint = reverse('cart:detail')
        self.assertIn(endpoint, self.browser.current_url)

        cart_item =  self.wait.until(lambda driver: driver.find_element(By.CSS_SELECTOR, ".cart tbody tr td a:first-of-type"))
        self.assertIn("Prochazka", cart_item.text)

        # The visitor decides to remove item from cart


        # The visitor gets the flash message that the event has been removed from the cart
        # but stays on the same cart detail page


        # The visitor can see that the cart is empty


        # The visitor can not see the REMOVE button


        # The visitor click the BACK button and is redirected to event detail page


        # The visitor decides to add more events than the number of available seats


        # The visitor decides to add more events than the number of available seats


        # The visitor gets the flash message that it is not possible to add more events than the number of seats available
        # and stays on the same event detail page
