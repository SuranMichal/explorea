

class LoggedInUserTest(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        # fix login
        super().setUpClass()
        cls.username = 'TEST'
        cls.password = 'password123'
        cls.user = UserModel.objects.create_user(username=cls.username,
                                                 password=cls.password)

#
#

    def setUp(self):
        self.client.login(username=self.username, password=self.password)
