from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView, FormView
from django.views.generic.edit import DeleteView
from django.views.generic.detail import SingleObjectMixin
from django.contrib import messages

from explorea.events.models import Event, EventRun
from .forms import CartAddForm
from .cart import Cart

class CartDetailView(TemplateView):
    template_name = "cart/cart_detail.html"

    # def get_context_data(self):     # tuto metodu nahrazuje middleware
    #     context = super().get_context_data() # ContextMixin.get_context_data()
    #     cart = Cart(self.request.session) # cart --> {'17': {'quantity': 2}, '22': {'quantity': 1}}
    #
    #     for item in cart: # for cyklus spusti cart.__iter__() ktera prida vsem product_id slovnikum klic "object", dale postupne pri kazde iteraci pridava klic "total_price"
    #                     # item --> {'quantity': 1, 'object': <EventRun: 2019-03-18|CzechOutdoor (FN)>, 'total_price': Decimal('19.00')}
    #         form_initial = {
    #             'quantity':item['quantity'],
    #             'update': True
    #         }
    #         item['cartadd_form'] = CartAddForm(initial=form_initial)
    #                     # item --> {'quantity': 1, 'object': <EventRun: 2019-03-18|CzechOutdoor (FN)>, 'total_price': Decimal('19.00'), 'cartadd_form': <CartAddForm bound=False, valid=Unknown, fields=(quantity;product_id;update;current_quantity)>}
    #     context['cart'] = cart
    #                     # cart --> {'17': {'quantity': 2, 'object': <EventRun: 2019-05-08|CzechOutdoor (FN)>, 'total_price': Decimal('4.00'), 'cartadd_form': <CartAddForm bound=False, valid=Unknown, fields=(quantity;product_id;update;current_quantity)>}, '22': {'quantity': 1, 'object': <EventRun: 2019-03-18|CzechOutdoor (FN)>, 'total_price': Decimal('19.00'), 'cartadd_form': <..>}}
    #     return context


class CartAddView (SingleObjectMixin, FormView):

    model = Event
    form_class =  CartAddForm
    template_name = 'events/event_detail.html' # chceme, abychom po pridani polozky do kosiku zustali na stejne strance -->
    slug_url_kwarg = 'slug' # --> tim padem nam vstupuje do view slug eventu z URLconf

    def post(self, request, *args, **kwargs):
        self.object = self.get_object() # ziska objekt na zaklade slug_url_kwarg
        # self.cart = Cart(self.request.session) # je v middleware # vytvori novy Cart objekt a da mu aktualni session; nova session se zahaji pri log out->log in, potom self.cart.cart --> {}
        return super().post(request, *args, **kwargs) #  ProcessFormView.post() - get_form() a rozdvojka form_valid(), form_invalid()

    def get_success_url(self):
        return self.object.get_absolute_url() # URL eventu --> potrebujeme zisakt self.object, abychom mohli ziskat stranku event detail

    def form_valid(self, form):

        self.request.cart.add (**form.cleaned_data) # odkazuje na request.cart, ktery prichazi z middleware # napr. form.cleaned_data = {'quantity': 1, 'product_id': '12'}, kde 12 je event.eventrun.id
        # self.cart.add (**form.cleaned_data)

        if form.cleaned_data['update']: # pokud form.cleaned_data['update'] = True, tedy value neni False ani None
            messages.success(self.request, 'Cart has been updated')
            return redirect('cart:detail')
        else:
            messages.success(self.request, 'The run has been added to the cart')
            return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):

        if form.cleaned_data['update']:
            messages.error(self.request, 'The cart could not be updated')
            return redirect('cart:detail')
        else:
            messages.error(self.request, 'The run could not be added into the cart')
            return HttpResponseRedirect(self.get_success_url())

# class CartRemoveView(View):
#
#     def get(self, request, *args, **kwargs): # View nedefinuje get(), tj. dispatch() spusti tuto metodu, kde musi byt definovano vse, a pak exekuce kodu skonci
#        # cart = Cart(self.request.session) # je v middleware
#        # cart.remove(self.kwargs['product_id'])
#         self.request.cart.remove(self.kwargs['product_id'])
#         return redirect('cart:detail')


def cart_remove(request, product_id):

    # cart = Cart(request.session) # je v middleware
    request.cart.remove (product_id) # odkazuje na request.cart, ktery prichazi z middleware

    return redirect("cart:detail")
