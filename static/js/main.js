// $(document).ready(function(){ // $(document).ready(() - pokud je nactena cela stranka
//     console.log("RUNNING MAIN JS");
//   $('.owl-carousel').owlCarousel({
//       loop:true,
//       margin:10,
//       nav:true,
//       dots:false,
//       autoHeight:true,
//       autoWidth: true,
//   });
// });

// // v JS se pracuje s eventy: napr. kdyz se najede mysi, kdyz se klikne na, atd., mj. i tzv. onchange (JQuery metoda .change()) - kdyz klikneme do jineho pole/jdeme pryc z pole username
// // $ = JQuery - kolekce casto pouzivanych metod napsana v JS - $("#id_username") je vyhledavani podle elementu - tj. melo by odpovidat document.getElementById('id_username') ??
// // .val(), .after() - dalsi metody JQuery
//
// $("#id_username").change(() => { // kdyz se zmeni hodnota objektu s id_username, tak se spusti nasledujici kod (az po console.log)
//                                   // change() je jakoby lambda funkce, ktera nebere zadny vstup a provadi vse co je ve slozenych zavorkach
//     console.log("DOING VALIDATION");
//     let form = document.getElementById('register-form'); // identifikace formulare podle id = register-form, let - deklarace promenne
//     let field = $("#id_username");
//
//     $.ajax({ // JQuery ma metodu ajax - ta bere objekt (ve slozenych zavorkach) s atributy url, data,...
//         url: form.getAttribute('data-username-validation'), // URL, kam ma byt poslan XHR request; vyhleda atribut HTML elementu form s nazvem data-username-validation;
//                                                             // v HTML template definujeme <form id='register-form' method="post" data-username-validation="{% url 'accounts:validate_username' %}">
//         data: { // reprezentuje data posilana v request.POST, ve forme slovniku; 'csrfmiddlewaretoken' musi byt obsazen, aby request.POST byl Djangem posouzen jako validni
//             'username': field.val(), // field je JQuery objekt - HTML element, .val() ziska hodnotu elementu (pokud jich splnuje vice, vzdy prvniho) - napr. Michal
//             'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(), // hodnota elementu, ktereho atribut name=csrfmiddlewaretoken
//         },
//         type: "POST", // HTTP metoda
//         dataType: 'json', // format, do ktereho budou data kodovana pred poslanim pres internet; referencuje content-type hlavicku na requestu, v JQuery staci zadat json misto application/json
//
//         success: (json) => { // success - pripad, kdy se response vrati v poradku ze serveru; definuje funkci, ktera bere vstup, ktery nazveme json - predstavuje samotnou response ze serveru - odpovida ve wiews JsonResponse(data)
//             if (json.exists) { // pokud atribut exists, ktery se vytvoril ve view na json, je True - tj. JsonResponse(data["exists"]) == True
//                 field.after(`<span class='error'>${json.error_message}</span>`); // za (.after) policko username se vlozi span element 'error' obsahujici error message - ve views JsonResponse(data["error_message"])
//             }
//         },
//
//         error: (jqXHR, err_desc, exc_obj) => { // v pripade erroru vypise objekt chyby, ktery posle server, do konzole prohlizece (console.log) - F12 Inspektor-->Console
//             console.log(exc_obj);
//         },
//
//     });
// });            // => tento blok nahrazen nasledujicim blokem kodu


$(document).ready(() => { // jakmile je nactena cela stranka
    setup_carousel();
    if (window.location.pathname.match('/.*register.*/')) {  // jakmile se nachazime na strance, ktera ma v URL string register

        $("#id_username").change(() => {
            check_field_value_exists(
                form_cls='register-form', // odkazuje na: <form id='register-form' .. > v register.html
                field_identifier="#id_username", // odkazuje na: {{ field.label_tag}} v register.html
                validation_attr='data-validation' // odkazuje na: <form .. data-validation="{% url 'accounts:validate_register_form' %}"> v register.html
            );
        });

        $("#id_email").change(() => { // jakmile se zmeni hodnota fieldu s id=id_email, spusti se funkce check_field_value_exists; tato funkce nesmi byt primo vstupem do change, aby se nespustila hned
            check_field_value_exists(
                form_cls='register-form', // argumenty funkce - viz nize
                field_identifier="#id_email",
                validation_attr='data-validation'
            );
        });
    }
}); // zde mame 2 spusteni funkce check_field_value_exists - jednou s field_identifier="#id_username", podruhe s field_identifier="#id_email". Zde je definovano,
    // ze request.POST se posle pri zmene prave poli formulare username a email --> validace tedy probiha pro tyto fieldy


function setup_carousel() {
    $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            dots:false,
            autoHeight:true,
            autoWidth: true,
        })
}


function check_field_value_exists(form_cls, field_identifier, validation_attr){  // form_cls, field_identifier, validation_attr - parametry funkce - viz vyse
    let form = document.getElementById(form_cls);
    let field = $(field_identifier);

    let err = field.next('.error'); // ziska element, ktery se nachazi za danym polem (coz je pripadny error)
    if (err) {err.remove()}; // pokud err existuje, tak ho odstrani -> bez techto dvou radku by se errory jen pridavaly a neodstranovaly, prejdeme-li na jine pole

    $.ajax({
        url: form.getAttribute(validation_attr),
        data: {
            'field_name': field_identifier.replace('#id_', ''), // napr. "#id_username" --> "username"
            'field_value': field.val(),
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
        },
        type: "POST",
        dataType: 'json',

        success: (json) => {
            if (json.exists) {
                field.after(`<span   class='error'>${json.error_message}</span>`);
            }
        },

        error: (jqXHR, err_desc, exc_obj) => {
            console.log(exc_obj);
        },

    });
}
