owl.carousel.min.css a owl.theme.default.min.css ziskany z OwlCarousel2-2.3.4.zip stazeneho z https://owlcarousel2.github.io/OwlCarousel2/index.html,
po rozbaleni z cesty: dist/assets/

owl.carousel.min.js po rozbaleni z cesty: dist/

owlCarousel je jQuery plugin
jQuery je js knihovna, umoznujici pristupovat k js elementum v html kodu stranky, menit jejich chovani

k owlCarousel je na webu docs - dokumentace
-  nastaveni elementu v options

      $(document).ready(function(){          # inicializacni funkce
        $(".owl-carousel").owlCarousel();
      });

      {   loop:true,                         # parametry inicializacni funkce
          margin:10,
          nav:true,
          dots:false,
          autoHeight:true,
          autoWidth: true,  }

      loop:true - obrazky se previji stale dokola (po poslednim se zobrazi prvni)
      nav:true - ukaze tlacitka dalsi/predchozi
      dots:false - tecky jako navigacni tlacitko se nebudou ukazovat


git config core.autocrlf
git config --local core.autocrlf false  
